﻿namespace Nha_sach_506
{
    partial class frmXuatHoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label13 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.txtTienNo_xhd = new System.Windows.Forms.TextBox();
            this.txtTienThoi_xhd = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.txtTienNhan_xhd = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.txtTongCong_xhd = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.dgvHoaDon_xhd = new System.Windows.Forms.DataGridView();
            this.lblThoiGian_xhd = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.lblDate_xhd = new System.Windows.Forms.Label();
            this.picHoaDon_xhd = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoaDon_xhd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHoaDon_xhd)).BeginInit();
            this.SuspendLayout();
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.ForeColor = System.Drawing.Color.Black;
            this.Label13.Location = new System.Drawing.Point(394, 721);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(221, 15);
            this.Label13.TabIndex = 42;
            this.Label13.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(225, 701);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(143, 16);
            this.Label10.TabIndex = 39;
            this.Label10.Text = "Chúc quý khách vui vẻ.";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(126, 676);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(331, 16);
            this.Label9.TabIndex = 38;
            this.Label9.Text = "Xin cám ơn quý khách đã mua hàng tại 506 BookStore.";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(536, 639);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(38, 16);
            this.Label12.TabIndex = 36;
            this.Label12.Text = "Đồng";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(536, 597);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(38, 16);
            this.Label8.TabIndex = 37;
            this.Label8.Text = "Đồng";
            // 
            // txtTienNo_xhd
            // 
            this.txtTienNo_xhd.BackColor = System.Drawing.Color.Beige;
            this.txtTienNo_xhd.Enabled = false;
            this.txtTienNo_xhd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienNo_xhd.Location = new System.Drawing.Point(414, 635);
            this.txtTienNo_xhd.Name = "txtTienNo_xhd";
            this.txtTienNo_xhd.Size = new System.Drawing.Size(108, 24);
            this.txtTienNo_xhd.TabIndex = 34;
            // 
            // txtTienThoi_xhd
            // 
            this.txtTienThoi_xhd.BackColor = System.Drawing.Color.Beige;
            this.txtTienThoi_xhd.Enabled = false;
            this.txtTienThoi_xhd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThoi_xhd.Location = new System.Drawing.Point(414, 593);
            this.txtTienThoi_xhd.Name = "txtTienThoi_xhd";
            this.txtTienThoi_xhd.Size = new System.Drawing.Size(108, 24);
            this.txtTienThoi_xhd.TabIndex = 35;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(237, 639);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(38, 16);
            this.Label7.TabIndex = 33;
            this.Label7.Text = "Đồng";
            // 
            // txtTienNhan_xhd
            // 
            this.txtTienNhan_xhd.BackColor = System.Drawing.Color.Beige;
            this.txtTienNhan_xhd.Enabled = false;
            this.txtTienNhan_xhd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienNhan_xhd.Location = new System.Drawing.Point(115, 635);
            this.txtTienNhan_xhd.Name = "txtTienNhan_xhd";
            this.txtTienNhan_xhd.Size = new System.Drawing.Size(108, 24);
            this.txtTienNhan_xhd.TabIndex = 32;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(237, 597);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 16);
            this.Label6.TabIndex = 31;
            this.Label6.Text = "Đồng";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(316, 639);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(60, 16);
            this.Label11.TabIndex = 28;
            this.Label11.Text = "Tiền nợ :";
            // 
            // txtTongCong_xhd
            // 
            this.txtTongCong_xhd.BackColor = System.Drawing.Color.Beige;
            this.txtTongCong_xhd.Enabled = false;
            this.txtTongCong_xhd.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongCong_xhd.Location = new System.Drawing.Point(115, 593);
            this.txtTongCong_xhd.Name = "txtTongCong_xhd";
            this.txtTongCong_xhd.Size = new System.Drawing.Size(108, 24);
            this.txtTongCong_xhd.TabIndex = 30;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(316, 597);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(65, 16);
            this.Label5.TabIndex = 29;
            this.Label5.Text = "Tiền thối :";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(17, 639);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(72, 16);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "Tiền nhận :";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(17, 597);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(76, 16);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "Tổng cộng :";
            // 
            // dgvHoaDon_xhd
            // 
            this.dgvHoaDon_xhd.BackgroundColor = System.Drawing.Color.Beige;
            this.dgvHoaDon_xhd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHoaDon_xhd.Location = new System.Drawing.Point(12, 257);
            this.dgvHoaDon_xhd.Name = "dgvHoaDon_xhd";
            this.dgvHoaDon_xhd.Size = new System.Drawing.Size(593, 317);
            this.dgvHoaDon_xhd.TabIndex = 25;
            // 
            // lblThoiGian_xhd
            // 
            this.lblThoiGian_xhd.AutoSize = true;
            this.lblThoiGian_xhd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiGian_xhd.Location = new System.Drawing.Point(12, 228);
            this.lblThoiGian_xhd.Name = "lblThoiGian_xhd";
            this.lblThoiGian_xhd.Size = new System.Drawing.Size(45, 17);
            this.lblThoiGian_xhd.TabIndex = 24;
            this.lblThoiGian_xhd.Text = "Ngày:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label1.Location = new System.Drawing.Point(216, 135);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(311, 34);
            this.Label1.TabIndex = 23;
            this.Label1.Text = "HÓA ĐƠN BÁN HÀNG";
            // 
            // lblDate_xhd
            // 
            this.lblDate_xhd.AutoSize = true;
            this.lblDate_xhd.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate_xhd.Location = new System.Drawing.Point(75, 226);
            this.lblDate_xhd.Name = "lblDate_xhd";
            this.lblDate_xhd.Size = new System.Drawing.Size(45, 20);
            this.lblDate_xhd.TabIndex = 43;
            this.lblDate_xhd.Text = "label2";
            // 
            // picHoaDon_xhd
            // 
            this.picHoaDon_xhd.Image = global::Nha_sach_506.Properties.Resources._506_LOGO1;
            this.picHoaDon_xhd.Location = new System.Drawing.Point(59, 12);
            this.picHoaDon_xhd.Name = "picHoaDon_xhd";
            this.picHoaDon_xhd.Size = new System.Drawing.Size(472, 169);
            this.picHoaDon_xhd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHoaDon_xhd.TabIndex = 22;
            this.picHoaDon_xhd.TabStop = false;
            // 
            // frmXuatHoaDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 733);
            this.Controls.Add(this.lblDate_xhd);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.txtTienNo_xhd);
            this.Controls.Add(this.txtTienThoi_xhd);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.txtTienNhan_xhd);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.txtTongCong_xhd);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.dgvHoaDon_xhd);
            this.Controls.Add(this.lblThoiGian_xhd);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.picHoaDon_xhd);
            this.Name = "frmXuatHoaDon";
            this.Text = "Hóa đơn bán hàng";
            this.Load += new System.EventHandler(this.frmXuatHoaDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoaDon_xhd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picHoaDon_xhd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox txtTienNo_xhd;
        internal System.Windows.Forms.TextBox txtTienThoi_xhd;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox txtTienNhan_xhd;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox txtTongCong_xhd;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.DataGridView dgvHoaDon_xhd;
        internal System.Windows.Forms.Label lblThoiGian_xhd;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.PictureBox picHoaDon_xhd;
        private System.Windows.Forms.Label lblDate_xhd;
    }
}