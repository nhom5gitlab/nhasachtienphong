﻿namespace Nha_sach_506
{
    partial class frmThuNgan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmThuNgan));
            this.lblDate = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.lblChucVu = new System.Windows.Forms.Label();
            this.lblTenNguoiDung = new System.Windows.Forms.Label();
            this.tabNhaSach = new System.Windows.Forms.TabControl();
            this.tabTrangChu = new System.Windows.Forms.TabPage();
            this.picTrangChu = new System.Windows.Forms.PictureBox();
            this.tabTraCuu = new System.Windows.Forms.TabPage();
            this.grpKhachHang_tc = new System.Windows.Forms.GroupBox();
            this.rbtKH_tc = new System.Windows.Forms.RadioButton();
            this.chkKhachNo = new System.Windows.Forms.CheckBox();
            this.grpHoaDon_tc = new System.Windows.Forms.GroupBox();
            this.rbtHD_tc = new System.Windows.Forms.RadioButton();
            this.grpSach_tc = new System.Windows.Forms.GroupBox();
            this.rbtS_tc = new System.Windows.Forms.RadioButton();
            this.chkConHang = new System.Windows.Forms.CheckBox();
            this.dgvTraCuu = new System.Windows.Forms.DataGridView();
            this.txtTuKhoa = new System.Windows.Forms.TextBox();
            this.lblTuKhoa = new System.Windows.Forms.Label();
            this.picTraCuu = new System.Windows.Forms.PictureBox();
            this.PictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabHoaDon = new System.Windows.Forms.TabPage();
            this.dgvHoaDon = new System.Windows.Forms.DataGridView();
            this.grpNhapHoaDon = new System.Windows.Forms.GroupBox();
            this.lblDate_hd = new System.Windows.Forms.Label();
            this.cboTenSach_hd = new System.Windows.Forms.ComboBox();
            this.lblNgayThang_hd = new System.Windows.Forms.Label();
            this.numSoLuong_hd = new System.Windows.Forms.NumericUpDown();
            this.lblSoLuong_hd = new System.Windows.Forms.Label();
            this.lblTenSach_hd = new System.Windows.Forms.Label();
            this.picNhapHoaDon = new System.Windows.Forms.PictureBox();
            this.grpThanhToan = new System.Windows.Forms.GroupBox();
            this.btnDangKyNo = new System.Windows.Forms.Button();
            this.lblDong_tt = new System.Windows.Forms.Label();
            this.lblDong_tn = new System.Windows.Forms.Label();
            this.lblDong_tc = new System.Windows.Forms.Label();
            this.lblTienThoi = new System.Windows.Forms.Label();
            this.txtTienThoi_hd = new System.Windows.Forms.TextBox();
            this.txtTienNhan_hd = new System.Windows.Forms.TextBox();
            this.lblTienNhan_hd = new System.Windows.Forms.Label();
            this.txtTongCong_hd = new System.Windows.Forms.TextBox();
            this.lblTongCong_hd = new System.Windows.Forms.Label();
            this.picThanhToan = new System.Windows.Forms.PictureBox();
            this.btnXuatHoaDon_hd = new System.Windows.Forms.Button();
            this.btnTinhTien_hd = new System.Windows.Forms.Button();
            this.btnXoaSach_hd = new System.Windows.Forms.Button();
            this.btnXoa_hd = new System.Windows.Forms.Button();
            this.btnThem_hd = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnHuyBo_kh = new System.Windows.Forms.Button();
            this.btnDangKy_kh = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.lblDienThoai = new System.Windows.Forms.Label();
            this.lblDiaChi = new System.Windows.Forms.Label();
            this.txtMaKhachHang = new System.Windows.Forms.TextBox();
            this.lblMaKhachHang = new System.Windows.Forms.Label();
            this.txtHoTen = new System.Windows.Forms.TextBox();
            this.lblHoTen = new System.Windows.Forms.Label();
            this.lblDkkhtt = new System.Windows.Forms.Label();
            this.picDkkhtt = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTienNo = new System.Windows.Forms.TextBox();
            this.btnXoaNo = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDate_ttno = new System.Windows.Forms.Label();
            this.lblThoiGian_khn = new System.Windows.Forms.Label();
            this.picKhNo = new System.Windows.Forms.PictureBox();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabNhaSach.SuspendLayout();
            this.tabTrangChu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).BeginInit();
            this.tabTraCuu.SuspendLayout();
            this.grpKhachHang_tc.SuspendLayout();
            this.grpHoaDon_tc.SuspendLayout();
            this.grpSach_tc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTraCuu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).BeginInit();
            this.tabHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoaDon)).BeginInit();
            this.grpNhapHoaDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoLuong_hd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNhapHoaDon)).BeginInit();
            this.grpThanhToan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picThanhToan)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDkkhtt)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKhNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(811, 111);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(45, 20);
            this.lblDate.TabIndex = 51;
            this.lblDate.Text = "label1";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(661, 726);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(221, 15);
            this.Label4.TabIndex = 49;
            this.Label4.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // lblChucVu
            // 
            this.lblChucVu.AutoSize = true;
            this.lblChucVu.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChucVu.Location = new System.Drawing.Point(720, 113);
            this.lblChucVu.Name = "lblChucVu";
            this.lblChucVu.Size = new System.Drawing.Size(64, 17);
            this.lblChucVu.TabIndex = 48;
            this.lblChucVu.Text = "Chức vụ";
            // 
            // lblTenNguoiDung
            // 
            this.lblTenNguoiDung.AutoSize = true;
            this.lblTenNguoiDung.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiDung.Location = new System.Drawing.Point(588, 113);
            this.lblTenNguoiDung.Name = "lblTenNguoiDung";
            this.lblTenNguoiDung.Size = new System.Drawing.Size(112, 17);
            this.lblTenNguoiDung.TabIndex = 47;
            this.lblTenNguoiDung.Text = "Tên người dùng";
            // 
            // tabNhaSach
            // 
            this.tabNhaSach.Controls.Add(this.tabTrangChu);
            this.tabNhaSach.Controls.Add(this.tabTraCuu);
            this.tabNhaSach.Controls.Add(this.tabHoaDon);
            this.tabNhaSach.Controls.Add(this.tabPage1);
            this.tabNhaSach.Controls.Add(this.tabPage2);
            this.tabNhaSach.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabNhaSach.Location = new System.Drawing.Point(1, 148);
            this.tabNhaSach.Name = "tabNhaSach";
            this.tabNhaSach.SelectedIndex = 0;
            this.tabNhaSach.Size = new System.Drawing.Size(881, 529);
            this.tabNhaSach.TabIndex = 46;
            // 
            // tabTrangChu
            // 
            this.tabTrangChu.Controls.Add(this.picTrangChu);
            this.tabTrangChu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTrangChu.Location = new System.Drawing.Point(4, 28);
            this.tabTrangChu.Name = "tabTrangChu";
            this.tabTrangChu.Padding = new System.Windows.Forms.Padding(3);
            this.tabTrangChu.Size = new System.Drawing.Size(873, 497);
            this.tabTrangChu.TabIndex = 0;
            this.tabTrangChu.Text = "Trang chủ";
            this.tabTrangChu.UseVisualStyleBackColor = true;
            // 
            // picTrangChu
            // 
            this.picTrangChu.Image = global::Nha_sach_506.Properties.Resources._506_LOGO;
            this.picTrangChu.Location = new System.Drawing.Point(111, 105);
            this.picTrangChu.Name = "picTrangChu";
            this.picTrangChu.Size = new System.Drawing.Size(658, 245);
            this.picTrangChu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTrangChu.TabIndex = 0;
            this.picTrangChu.TabStop = false;
            // 
            // tabTraCuu
            // 
            this.tabTraCuu.Controls.Add(this.grpKhachHang_tc);
            this.tabTraCuu.Controls.Add(this.grpHoaDon_tc);
            this.tabTraCuu.Controls.Add(this.grpSach_tc);
            this.tabTraCuu.Controls.Add(this.dgvTraCuu);
            this.tabTraCuu.Controls.Add(this.txtTuKhoa);
            this.tabTraCuu.Controls.Add(this.lblTuKhoa);
            this.tabTraCuu.Controls.Add(this.picTraCuu);
            this.tabTraCuu.Controls.Add(this.PictureBox4);
            this.tabTraCuu.Location = new System.Drawing.Point(4, 28);
            this.tabTraCuu.Name = "tabTraCuu";
            this.tabTraCuu.Padding = new System.Windows.Forms.Padding(3);
            this.tabTraCuu.Size = new System.Drawing.Size(873, 497);
            this.tabTraCuu.TabIndex = 2;
            this.tabTraCuu.Text = "Tra cứu";
            this.tabTraCuu.UseVisualStyleBackColor = true;
            this.tabTraCuu.Click += new System.EventHandler(this.tabTraCuu_Click);
            // 
            // grpKhachHang_tc
            // 
            this.grpKhachHang_tc.Controls.Add(this.rbtKH_tc);
            this.grpKhachHang_tc.Controls.Add(this.chkKhachNo);
            this.grpKhachHang_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpKhachHang_tc.Location = new System.Drawing.Point(427, 81);
            this.grpKhachHang_tc.Name = "grpKhachHang_tc";
            this.grpKhachHang_tc.Size = new System.Drawing.Size(158, 80);
            this.grpKhachHang_tc.TabIndex = 43;
            this.grpKhachHang_tc.TabStop = false;
            this.grpKhachHang_tc.Text = "Tra cứu Khách hàng";
            // 
            // rbtKH_tc
            // 
            this.rbtKH_tc.AutoSize = true;
            this.rbtKH_tc.Location = new System.Drawing.Point(15, 21);
            this.rbtKH_tc.Name = "rbtKH_tc";
            this.rbtKH_tc.Size = new System.Drawing.Size(102, 20);
            this.rbtKH_tc.TabIndex = 44;
            this.rbtKH_tc.TabStop = true;
            this.rbtKH_tc.Text = "Khách hàng";
            this.rbtKH_tc.UseVisualStyleBackColor = true;
            // 
            // chkKhachNo
            // 
            this.chkKhachNo.AutoSize = true;
            this.chkKhachNo.Location = new System.Drawing.Point(15, 47);
            this.chkKhachNo.Name = "chkKhachNo";
            this.chkKhachNo.Size = new System.Drawing.Size(88, 20);
            this.chkKhachNo.TabIndex = 12;
            this.chkKhachNo.Text = "Khách nợ";
            this.chkKhachNo.UseVisualStyleBackColor = true;
            // 
            // grpHoaDon_tc
            // 
            this.grpHoaDon_tc.Controls.Add(this.rbtHD_tc);
            this.grpHoaDon_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpHoaDon_tc.Location = new System.Drawing.Point(229, 81);
            this.grpHoaDon_tc.Name = "grpHoaDon_tc";
            this.grpHoaDon_tc.Size = new System.Drawing.Size(158, 80);
            this.grpHoaDon_tc.TabIndex = 42;
            this.grpHoaDon_tc.TabStop = false;
            this.grpHoaDon_tc.Text = "Tra cứu Hóa đơn";
            // 
            // rbtHD_tc
            // 
            this.rbtHD_tc.AutoSize = true;
            this.rbtHD_tc.Location = new System.Drawing.Point(23, 21);
            this.rbtHD_tc.Name = "rbtHD_tc";
            this.rbtHD_tc.Size = new System.Drawing.Size(80, 20);
            this.rbtHD_tc.TabIndex = 44;
            this.rbtHD_tc.TabStop = true;
            this.rbtHD_tc.Text = "Hoá đơn";
            this.rbtHD_tc.UseVisualStyleBackColor = true;
            // 
            // grpSach_tc
            // 
            this.grpSach_tc.Controls.Add(this.rbtS_tc);
            this.grpSach_tc.Controls.Add(this.chkConHang);
            this.grpSach_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSach_tc.Location = new System.Drawing.Point(36, 81);
            this.grpSach_tc.Name = "grpSach_tc";
            this.grpSach_tc.Size = new System.Drawing.Size(158, 80);
            this.grpSach_tc.TabIndex = 41;
            this.grpSach_tc.TabStop = false;
            this.grpSach_tc.Text = "Tra cứu Sách";
            // 
            // rbtS_tc
            // 
            this.rbtS_tc.AutoSize = true;
            this.rbtS_tc.Location = new System.Drawing.Point(19, 21);
            this.rbtS_tc.Name = "rbtS_tc";
            this.rbtS_tc.Size = new System.Drawing.Size(58, 20);
            this.rbtS_tc.TabIndex = 44;
            this.rbtS_tc.TabStop = true;
            this.rbtS_tc.Text = "Sách";
            this.rbtS_tc.UseVisualStyleBackColor = true;
            // 
            // chkConHang
            // 
            this.chkConHang.AutoSize = true;
            this.chkConHang.Location = new System.Drawing.Point(19, 47);
            this.chkConHang.Name = "chkConHang";
            this.chkConHang.Size = new System.Drawing.Size(88, 20);
            this.chkConHang.TabIndex = 10;
            this.chkConHang.Text = "Còn hàng";
            this.chkConHang.UseVisualStyleBackColor = true;
            this.chkConHang.CheckedChanged += new System.EventHandler(this.chkConHang_CheckedChanged);
            // 
            // dgvTraCuu
            // 
            this.dgvTraCuu.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvTraCuu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTraCuu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvTraCuu.Location = new System.Drawing.Point(3, 185);
            this.dgvTraCuu.Name = "dgvTraCuu";
            this.dgvTraCuu.Size = new System.Drawing.Size(867, 309);
            this.dgvTraCuu.TabIndex = 11;
            // 
            // txtTuKhoa
            // 
            this.txtTuKhoa.Location = new System.Drawing.Point(21, 42);
            this.txtTuKhoa.Name = "txtTuKhoa";
            this.txtTuKhoa.Size = new System.Drawing.Size(464, 26);
            this.txtTuKhoa.TabIndex = 8;
            // 
            // lblTuKhoa
            // 
            this.lblTuKhoa.AutoSize = true;
            this.lblTuKhoa.Location = new System.Drawing.Point(17, 20);
            this.lblTuKhoa.Name = "lblTuKhoa";
            this.lblTuKhoa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTuKhoa.Size = new System.Drawing.Size(83, 19);
            this.lblTuKhoa.TabIndex = 7;
            this.lblTuKhoa.Text = "Từ khóa :";
            // 
            // picTraCuu
            // 
            this.picTraCuu.Image = global::Nha_sach_506.Properties.Resources.stock_control;
            this.picTraCuu.Location = new System.Drawing.Point(713, 20);
            this.picTraCuu.Name = "picTraCuu";
            this.picTraCuu.Size = new System.Drawing.Size(113, 97);
            this.picTraCuu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTraCuu.TabIndex = 9;
            this.picTraCuu.TabStop = false;
            // 
            // PictureBox4
            // 
            this.PictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.PictureBox4.Location = new System.Drawing.Point(3, 3);
            this.PictureBox4.Name = "PictureBox4";
            this.PictureBox4.Size = new System.Drawing.Size(867, 176);
            this.PictureBox4.TabIndex = 6;
            this.PictureBox4.TabStop = false;
            // 
            // tabHoaDon
            // 
            this.tabHoaDon.Controls.Add(this.dgvHoaDon);
            this.tabHoaDon.Controls.Add(this.grpNhapHoaDon);
            this.tabHoaDon.Controls.Add(this.grpThanhToan);
            this.tabHoaDon.Controls.Add(this.btnXuatHoaDon_hd);
            this.tabHoaDon.Controls.Add(this.btnTinhTien_hd);
            this.tabHoaDon.Controls.Add(this.btnXoaSach_hd);
            this.tabHoaDon.Controls.Add(this.btnXoa_hd);
            this.tabHoaDon.Controls.Add(this.btnThem_hd);
            this.tabHoaDon.Location = new System.Drawing.Point(4, 28);
            this.tabHoaDon.Name = "tabHoaDon";
            this.tabHoaDon.Padding = new System.Windows.Forms.Padding(3);
            this.tabHoaDon.Size = new System.Drawing.Size(873, 497);
            this.tabHoaDon.TabIndex = 3;
            this.tabHoaDon.Text = "Hóa đơn";
            this.tabHoaDon.UseVisualStyleBackColor = true;
            // 
            // dgvHoaDon
            // 
            this.dgvHoaDon.BackgroundColor = System.Drawing.Color.White;
            this.dgvHoaDon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHoaDon.Location = new System.Drawing.Point(3, 276);
            this.dgvHoaDon.Name = "dgvHoaDon";
            this.dgvHoaDon.Size = new System.Drawing.Size(864, 215);
            this.dgvHoaDon.TabIndex = 41;
            // 
            // grpNhapHoaDon
            // 
            this.grpNhapHoaDon.BackColor = System.Drawing.Color.White;
            this.grpNhapHoaDon.Controls.Add(this.lblDate_hd);
            this.grpNhapHoaDon.Controls.Add(this.cboTenSach_hd);
            this.grpNhapHoaDon.Controls.Add(this.lblNgayThang_hd);
            this.grpNhapHoaDon.Controls.Add(this.numSoLuong_hd);
            this.grpNhapHoaDon.Controls.Add(this.lblSoLuong_hd);
            this.grpNhapHoaDon.Controls.Add(this.lblTenSach_hd);
            this.grpNhapHoaDon.Controls.Add(this.picNhapHoaDon);
            this.grpNhapHoaDon.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpNhapHoaDon.Location = new System.Drawing.Point(6, 7);
            this.grpNhapHoaDon.Name = "grpNhapHoaDon";
            this.grpNhapHoaDon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grpNhapHoaDon.Size = new System.Drawing.Size(404, 197);
            this.grpNhapHoaDon.TabIndex = 2;
            this.grpNhapHoaDon.TabStop = false;
            this.grpNhapHoaDon.Text = "Nhập hóa đơn";
            // 
            // lblDate_hd
            // 
            this.lblDate_hd.AutoSize = true;
            this.lblDate_hd.Location = new System.Drawing.Point(130, 129);
            this.lblDate_hd.Name = "lblDate_hd";
            this.lblDate_hd.Size = new System.Drawing.Size(49, 17);
            this.lblDate_hd.TabIndex = 8;
            this.lblDate_hd.Text = "label2";
            // 
            // cboTenSach_hd
            // 
            this.cboTenSach_hd.FormattingEnabled = true;
            this.cboTenSach_hd.Location = new System.Drawing.Point(132, 46);
            this.cboTenSach_hd.Name = "cboTenSach_hd";
            this.cboTenSach_hd.Size = new System.Drawing.Size(133, 25);
            this.cboTenSach_hd.TabIndex = 7;
            // 
            // lblNgayThang_hd
            // 
            this.lblNgayThang_hd.AutoSize = true;
            this.lblNgayThang_hd.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayThang_hd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayThang_hd.Location = new System.Drawing.Point(35, 129);
            this.lblNgayThang_hd.Name = "lblNgayThang_hd";
            this.lblNgayThang_hd.Size = new System.Drawing.Size(89, 17);
            this.lblNgayThang_hd.TabIndex = 5;
            this.lblNgayThang_hd.Text = "Ngày tháng :";
            // 
            // numSoLuong_hd
            // 
            this.numSoLuong_hd.Location = new System.Drawing.Point(132, 91);
            this.numSoLuong_hd.Name = "numSoLuong_hd";
            this.numSoLuong_hd.Size = new System.Drawing.Size(133, 25);
            this.numSoLuong_hd.TabIndex = 3;
            // 
            // lblSoLuong_hd
            // 
            this.lblSoLuong_hd.AutoSize = true;
            this.lblSoLuong_hd.BackColor = System.Drawing.Color.Transparent;
            this.lblSoLuong_hd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong_hd.Location = new System.Drawing.Point(35, 93);
            this.lblSoLuong_hd.Name = "lblSoLuong_hd";
            this.lblSoLuong_hd.Size = new System.Drawing.Size(77, 17);
            this.lblSoLuong_hd.TabIndex = 2;
            this.lblSoLuong_hd.Text = "Số lượng :";
            // 
            // lblTenSach_hd
            // 
            this.lblTenSach_hd.AutoSize = true;
            this.lblTenSach_hd.BackColor = System.Drawing.Color.Transparent;
            this.lblTenSach_hd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSach_hd.Location = new System.Drawing.Point(35, 49);
            this.lblTenSach_hd.Name = "lblTenSach_hd";
            this.lblTenSach_hd.Size = new System.Drawing.Size(77, 17);
            this.lblTenSach_hd.TabIndex = 0;
            this.lblTenSach_hd.Text = "Tên sách :";
            // 
            // picNhapHoaDon
            // 
            this.picNhapHoaDon.Image = global::Nha_sach_506.Properties.Resources.amb_logo;
            this.picNhapHoaDon.Location = new System.Drawing.Point(280, 61);
            this.picNhapHoaDon.Name = "picNhapHoaDon";
            this.picNhapHoaDon.Size = new System.Drawing.Size(117, 136);
            this.picNhapHoaDon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picNhapHoaDon.TabIndex = 4;
            this.picNhapHoaDon.TabStop = false;
            // 
            // grpThanhToan
            // 
            this.grpThanhToan.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.grpThanhToan.Controls.Add(this.btnDangKyNo);
            this.grpThanhToan.Controls.Add(this.lblDong_tt);
            this.grpThanhToan.Controls.Add(this.lblDong_tn);
            this.grpThanhToan.Controls.Add(this.lblDong_tc);
            this.grpThanhToan.Controls.Add(this.lblTienThoi);
            this.grpThanhToan.Controls.Add(this.txtTienThoi_hd);
            this.grpThanhToan.Controls.Add(this.txtTienNhan_hd);
            this.grpThanhToan.Controls.Add(this.lblTienNhan_hd);
            this.grpThanhToan.Controls.Add(this.txtTongCong_hd);
            this.grpThanhToan.Controls.Add(this.lblTongCong_hd);
            this.grpThanhToan.Controls.Add(this.picThanhToan);
            this.grpThanhToan.Location = new System.Drawing.Point(416, 7);
            this.grpThanhToan.Name = "grpThanhToan";
            this.grpThanhToan.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grpThanhToan.Size = new System.Drawing.Size(454, 197);
            this.grpThanhToan.TabIndex = 3;
            this.grpThanhToan.TabStop = false;
            this.grpThanhToan.Text = "Thanh toán";
            // 
            // btnDangKyNo
            // 
            this.btnDangKyNo.BackColor = System.Drawing.Color.Gray;
            this.btnDangKyNo.Location = new System.Drawing.Point(221, 148);
            this.btnDangKyNo.Name = "btnDangKyNo";
            this.btnDangKyNo.Size = new System.Drawing.Size(120, 39);
            this.btnDangKyNo.TabIndex = 13;
            this.btnDangKyNo.Text = "Đăng ký nợ";
            this.btnDangKyNo.UseVisualStyleBackColor = false;
            // 
            // lblDong_tt
            // 
            this.lblDong_tt.AutoSize = true;
            this.lblDong_tt.BackColor = System.Drawing.Color.Transparent;
            this.lblDong_tt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDong_tt.Location = new System.Drawing.Point(304, 112);
            this.lblDong_tt.Name = "lblDong_tt";
            this.lblDong_tt.Size = new System.Drawing.Size(43, 17);
            this.lblDong_tt.TabIndex = 12;
            this.lblDong_tt.Text = "Đồng";
            // 
            // lblDong_tn
            // 
            this.lblDong_tn.AutoSize = true;
            this.lblDong_tn.BackColor = System.Drawing.Color.Transparent;
            this.lblDong_tn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDong_tn.Location = new System.Drawing.Point(304, 71);
            this.lblDong_tn.Name = "lblDong_tn";
            this.lblDong_tn.Size = new System.Drawing.Size(43, 17);
            this.lblDong_tn.TabIndex = 11;
            this.lblDong_tn.Text = "Đồng";
            // 
            // lblDong_tc
            // 
            this.lblDong_tc.AutoSize = true;
            this.lblDong_tc.BackColor = System.Drawing.Color.Transparent;
            this.lblDong_tc.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDong_tc.Location = new System.Drawing.Point(304, 31);
            this.lblDong_tc.Name = "lblDong_tc";
            this.lblDong_tc.Size = new System.Drawing.Size(43, 17);
            this.lblDong_tc.TabIndex = 10;
            this.lblDong_tc.Text = "Đồng";
            // 
            // lblTienThoi
            // 
            this.lblTienThoi.AutoSize = true;
            this.lblTienThoi.BackColor = System.Drawing.Color.Transparent;
            this.lblTienThoi.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienThoi.Location = new System.Drawing.Point(27, 112);
            this.lblTienThoi.Name = "lblTienThoi";
            this.lblTienThoi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTienThoi.Size = new System.Drawing.Size(70, 17);
            this.lblTienThoi.TabIndex = 8;
            this.lblTienThoi.Text = "Tiền thối :";
            // 
            // txtTienThoi_hd
            // 
            this.txtTienThoi_hd.Enabled = false;
            this.txtTienThoi_hd.Location = new System.Drawing.Point(160, 109);
            this.txtTienThoi_hd.Name = "txtTienThoi_hd";
            this.txtTienThoi_hd.Size = new System.Drawing.Size(133, 26);
            this.txtTienThoi_hd.TabIndex = 9;
            // 
            // txtTienNhan_hd
            // 
            this.txtTienNhan_hd.Location = new System.Drawing.Point(160, 68);
            this.txtTienNhan_hd.Name = "txtTienNhan_hd";
            this.txtTienNhan_hd.Size = new System.Drawing.Size(133, 26);
            this.txtTienNhan_hd.TabIndex = 7;
            // 
            // lblTienNhan_hd
            // 
            this.lblTienNhan_hd.AutoSize = true;
            this.lblTienNhan_hd.BackColor = System.Drawing.Color.Transparent;
            this.lblTienNhan_hd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTienNhan_hd.Location = new System.Drawing.Point(27, 71);
            this.lblTienNhan_hd.Name = "lblTienNhan_hd";
            this.lblTienNhan_hd.Size = new System.Drawing.Size(79, 17);
            this.lblTienNhan_hd.TabIndex = 6;
            this.lblTienNhan_hd.Text = "Tiền nhận :";
            // 
            // txtTongCong_hd
            // 
            this.txtTongCong_hd.Enabled = false;
            this.txtTongCong_hd.Location = new System.Drawing.Point(160, 28);
            this.txtTongCong_hd.Name = "txtTongCong_hd";
            this.txtTongCong_hd.Size = new System.Drawing.Size(133, 26);
            this.txtTongCong_hd.TabIndex = 5;
            // 
            // lblTongCong_hd
            // 
            this.lblTongCong_hd.AutoSize = true;
            this.lblTongCong_hd.BackColor = System.Drawing.Color.Transparent;
            this.lblTongCong_hd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongCong_hd.ForeColor = System.Drawing.Color.Red;
            this.lblTongCong_hd.Location = new System.Drawing.Point(27, 31);
            this.lblTongCong_hd.Name = "lblTongCong_hd";
            this.lblTongCong_hd.Size = new System.Drawing.Size(106, 17);
            this.lblTongCong_hd.TabIndex = 4;
            this.lblTongCong_hd.Text = "TỔNG CỘNG :";
            // 
            // picThanhToan
            // 
            this.picThanhToan.Image = global::Nha_sach_506.Properties.Resources.icon_taxes;
            this.picThanhToan.Location = new System.Drawing.Point(369, 112);
            this.picThanhToan.Name = "picThanhToan";
            this.picThanhToan.Size = new System.Drawing.Size(79, 79);
            this.picThanhToan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picThanhToan.TabIndex = 4;
            this.picThanhToan.TabStop = false;
            // 
            // btnXuatHoaDon_hd
            // 
            this.btnXuatHoaDon_hd.BackColor = System.Drawing.Color.Crimson;
            this.btnXuatHoaDon_hd.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXuatHoaDon_hd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnXuatHoaDon_hd.Image = global::Nha_sach_506.Properties.Resources.Ecommerce_Bill_icon;
            this.btnXuatHoaDon_hd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXuatHoaDon_hd.Location = new System.Drawing.Point(637, 210);
            this.btnXuatHoaDon_hd.Name = "btnXuatHoaDon_hd";
            this.btnXuatHoaDon_hd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnXuatHoaDon_hd.Size = new System.Drawing.Size(188, 60);
            this.btnXuatHoaDon_hd.TabIndex = 40;
            this.btnXuatHoaDon_hd.Text = "Xuất hóa đơn";
            this.btnXuatHoaDon_hd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXuatHoaDon_hd.UseVisualStyleBackColor = false;
            // 
            // btnTinhTien_hd
            // 
            this.btnTinhTien_hd.BackColor = System.Drawing.Color.YellowGreen;
            this.btnTinhTien_hd.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTinhTien_hd.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnTinhTien_hd.Image = global::Nha_sach_506.Properties.Resources.payment_icon;
            this.btnTinhTien_hd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTinhTien_hd.Location = new System.Drawing.Point(446, 210);
            this.btnTinhTien_hd.Name = "btnTinhTien_hd";
            this.btnTinhTien_hd.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnTinhTien_hd.Size = new System.Drawing.Size(158, 60);
            this.btnTinhTien_hd.TabIndex = 39;
            this.btnTinhTien_hd.Text = "Tính tiền";
            this.btnTinhTien_hd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTinhTien_hd.UseVisualStyleBackColor = false;
            // 
            // btnXoaSach_hd
            // 
            this.btnXoaSach_hd.BackColor = System.Drawing.Color.LimeGreen;
            this.btnXoaSach_hd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXoaSach_hd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXoaSach_hd.Image = global::Nha_sach_506.Properties.Resources.Button_Refresh_icon;
            this.btnXoaSach_hd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaSach_hd.Location = new System.Drawing.Point(262, 215);
            this.btnXoaSach_hd.Name = "btnXoaSach_hd";
            this.btnXoaSach_hd.Size = new System.Drawing.Size(122, 53);
            this.btnXoaSach_hd.TabIndex = 38;
            this.btnXoaSach_hd.Text = "Xóa sạch";
            this.btnXoaSach_hd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoaSach_hd.UseVisualStyleBackColor = false;
            // 
            // btnXoa_hd
            // 
            this.btnXoa_hd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnXoa_hd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXoa_hd.Image = global::Nha_sach_506.Properties.Resources.cancel_icon;
            this.btnXoa_hd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoa_hd.Location = new System.Drawing.Point(138, 215);
            this.btnXoa_hd.Name = "btnXoa_hd";
            this.btnXoa_hd.Size = new System.Drawing.Size(89, 53);
            this.btnXoa_hd.TabIndex = 37;
            this.btnXoa_hd.Text = "Xóa";
            this.btnXoa_hd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoa_hd.UseVisualStyleBackColor = false;
            // 
            // btnThem_hd
            // 
            this.btnThem_hd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnThem_hd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnThem_hd.Image = global::Nha_sach_506.Properties.Resources.book_add_icon;
            this.btnThem_hd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThem_hd.Location = new System.Drawing.Point(20, 215);
            this.btnThem_hd.Name = "btnThem_hd";
            this.btnThem_hd.Size = new System.Drawing.Size(89, 53);
            this.btnThem_hd.TabIndex = 36;
            this.btnThem_hd.Text = "Thêm";
            this.btnThem_hd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnThem_hd.UseVisualStyleBackColor = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnHuyBo_kh);
            this.tabPage1.Controls.Add(this.btnDangKy_kh);
            this.tabPage1.Controls.Add(this.txtEmail);
            this.tabPage1.Controls.Add(this.lblEmail);
            this.tabPage1.Controls.Add(this.txtDienThoai);
            this.tabPage1.Controls.Add(this.txtDiaChi);
            this.tabPage1.Controls.Add(this.lblDienThoai);
            this.tabPage1.Controls.Add(this.lblDiaChi);
            this.tabPage1.Controls.Add(this.txtMaKhachHang);
            this.tabPage1.Controls.Add(this.lblMaKhachHang);
            this.tabPage1.Controls.Add(this.txtHoTen);
            this.tabPage1.Controls.Add(this.lblHoTen);
            this.tabPage1.Controls.Add(this.lblDkkhtt);
            this.tabPage1.Controls.Add(this.picDkkhtt);
            this.tabPage1.Location = new System.Drawing.Point(4, 28);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(873, 497);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Đăng ký khách hàng";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnHuyBo_kh
            // 
            this.btnHuyBo_kh.BackColor = System.Drawing.Color.Red;
            this.btnHuyBo_kh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHuyBo_kh.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuyBo_kh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHuyBo_kh.Location = new System.Drawing.Point(515, 354);
            this.btnHuyBo_kh.Name = "btnHuyBo_kh";
            this.btnHuyBo_kh.Size = new System.Drawing.Size(129, 43);
            this.btnHuyBo_kh.TabIndex = 50;
            this.btnHuyBo_kh.Text = "Hủy bỏ";
            this.btnHuyBo_kh.UseVisualStyleBackColor = false;
            // 
            // btnDangKy_kh
            // 
            this.btnDangKy_kh.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnDangKy_kh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDangKy_kh.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangKy_kh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDangKy_kh.Location = new System.Drawing.Point(358, 354);
            this.btnDangKy_kh.Name = "btnDangKy_kh";
            this.btnDangKy_kh.Size = new System.Drawing.Size(129, 43);
            this.btnDangKy_kh.TabIndex = 49;
            this.btnDangKy_kh.Text = "Đăng ký ";
            this.btnDangKy_kh.UseVisualStyleBackColor = false;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(449, 311);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(195, 25);
            this.txtEmail.TabIndex = 48;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(339, 316);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(53, 17);
            this.lblEmail.TabIndex = 47;
            this.lblEmail.Text = "Email :";
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDienThoai.Location = new System.Drawing.Point(449, 264);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(195, 25);
            this.txtDienThoai.TabIndex = 45;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChi.Location = new System.Drawing.Point(449, 216);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(195, 25);
            this.txtDiaChi.TabIndex = 46;
            // 
            // lblDienThoai
            // 
            this.lblDienThoai.AutoSize = true;
            this.lblDienThoai.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDienThoai.Location = new System.Drawing.Point(339, 269);
            this.lblDienThoai.Name = "lblDienThoai";
            this.lblDienThoai.Size = new System.Drawing.Size(81, 17);
            this.lblDienThoai.TabIndex = 44;
            this.lblDienThoai.Text = "Điện thoại :";
            // 
            // lblDiaChi
            // 
            this.lblDiaChi.AutoSize = true;
            this.lblDiaChi.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChi.Location = new System.Drawing.Point(339, 221);
            this.lblDiaChi.Name = "lblDiaChi";
            this.lblDiaChi.Size = new System.Drawing.Size(61, 17);
            this.lblDiaChi.TabIndex = 43;
            this.lblDiaChi.Text = "Địa chỉ :";
            // 
            // txtMaKhachHang
            // 
            this.txtMaKhachHang.Enabled = false;
            this.txtMaKhachHang.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaKhachHang.Location = new System.Drawing.Point(514, 120);
            this.txtMaKhachHang.Name = "txtMaKhachHang";
            this.txtMaKhachHang.Size = new System.Drawing.Size(129, 25);
            this.txtMaKhachHang.TabIndex = 42;
            // 
            // lblMaKhachHang
            // 
            this.lblMaKhachHang.AutoSize = true;
            this.lblMaKhachHang.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaKhachHang.Location = new System.Drawing.Point(399, 125);
            this.lblMaKhachHang.Name = "lblMaKhachHang";
            this.lblMaKhachHang.Size = new System.Drawing.Size(109, 17);
            this.lblMaKhachHang.TabIndex = 41;
            this.lblMaKhachHang.Text = "Mã khách hàng";
            // 
            // txtHoTen
            // 
            this.txtHoTen.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen.Location = new System.Drawing.Point(449, 169);
            this.txtHoTen.Name = "txtHoTen";
            this.txtHoTen.Size = new System.Drawing.Size(195, 25);
            this.txtHoTen.TabIndex = 40;
            // 
            // lblHoTen
            // 
            this.lblHoTen.AutoSize = true;
            this.lblHoTen.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoTen.Location = new System.Drawing.Point(339, 174);
            this.lblHoTen.Name = "lblHoTen";
            this.lblHoTen.Size = new System.Drawing.Size(82, 17);
            this.lblHoTen.TabIndex = 39;
            this.lblHoTen.Text = "Họ và Tên :";
            // 
            // lblDkkhtt
            // 
            this.lblDkkhtt.AutoSize = true;
            this.lblDkkhtt.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDkkhtt.Location = new System.Drawing.Point(231, 85);
            this.lblDkkhtt.Name = "lblDkkhtt";
            this.lblDkkhtt.Size = new System.Drawing.Size(375, 24);
            this.lblDkkhtt.TabIndex = 38;
            this.lblDkkhtt.Text = "ĐĂNG KÝ KHÁCH HÀNG THÂN THIẾT";
            // 
            // picDkkhtt
            // 
            this.picDkkhtt.Image = ((System.Drawing.Image)(resources.GetObject("picDkkhtt.Image")));
            this.picDkkhtt.Location = new System.Drawing.Point(179, 153);
            this.picDkkhtt.Name = "picDkkhtt";
            this.picDkkhtt.Size = new System.Drawing.Size(142, 150);
            this.picDkkhtt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picDkkhtt.TabIndex = 37;
            this.picDkkhtt.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.txtKey);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.lblDate_ttno);
            this.tabPage2.Controls.Add(this.lblThoiGian_khn);
            this.tabPage2.Controls.Add(this.picKhNo);
            this.tabPage2.Location = new System.Drawing.Point(4, 28);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(873, 497);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Thanh toán nợ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtTienNo);
            this.groupBox1.Controls.Add(this.btnXoaNo);
            this.groupBox1.Location = new System.Drawing.Point(248, 343);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(460, 111);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thanh toán nợ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 19);
            this.label5.TabIndex = 62;
            this.label5.Text = "Số tiền";
            // 
            // txtTienNo
            // 
            this.txtTienNo.Enabled = false;
            this.txtTienNo.Location = new System.Drawing.Point(15, 63);
            this.txtTienNo.Name = "txtTienNo";
            this.txtTienNo.Size = new System.Drawing.Size(191, 26);
            this.txtTienNo.TabIndex = 61;
            // 
            // btnXoaNo
            // 
            this.btnXoaNo.BackColor = System.Drawing.Color.Tomato;
            this.btnXoaNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXoaNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaNo.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaNo.Image")));
            this.btnXoaNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoaNo.Location = new System.Drawing.Point(228, 36);
            this.btnXoaNo.Name = "btnXoaNo";
            this.btnXoaNo.Size = new System.Drawing.Size(199, 53);
            this.btnXoaNo.TabIndex = 55;
            this.btnXoaNo.Text = "Lập phiếu thu tiền";
            this.btnXoaNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoaNo.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 229);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(818, 82);
            this.dataGridView1.TabIndex = 60;
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(352, 187);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(489, 26);
            this.txtKey.TabIndex = 59;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(259, 190);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(83, 19);
            this.label6.TabIndex = 58;
            this.label6.Text = "Từ khóa :";
            // 
            // lblDate_ttno
            // 
            this.lblDate_ttno.AutoSize = true;
            this.lblDate_ttno.Location = new System.Drawing.Point(103, 189);
            this.lblDate_ttno.Name = "lblDate_ttno";
            this.lblDate_ttno.Size = new System.Drawing.Size(54, 19);
            this.lblDate_ttno.TabIndex = 57;
            this.lblDate_ttno.Text = "label3";
            // 
            // lblThoiGian_khn
            // 
            this.lblThoiGian_khn.AutoSize = true;
            this.lblThoiGian_khn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiGian_khn.Location = new System.Drawing.Point(20, 190);
            this.lblThoiGian_khn.Name = "lblThoiGian_khn";
            this.lblThoiGian_khn.Size = new System.Drawing.Size(77, 17);
            this.lblThoiGian_khn.TabIndex = 53;
            this.lblThoiGian_khn.Text = "Thời gian :";
            this.lblThoiGian_khn.Click += new System.EventHandler(this.lblThoiGian_khn_Click);
            // 
            // picKhNo
            // 
            this.picKhNo.Image = ((System.Drawing.Image)(resources.GetObject("picKhNo.Image")));
            this.picKhNo.Location = new System.Drawing.Point(203, 6);
            this.picKhNo.Name = "picKhNo";
            this.picKhNo.Size = new System.Drawing.Size(472, 169);
            this.picKhNo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picKhNo.TabIndex = 51;
            this.picKhNo.TabStop = false;
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.BackColor = System.Drawing.Color.Tomato;
            this.btnDangXuat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDangXuat.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangXuat.Image = global::Nha_sach_506.Properties.Resources.exit;
            this.btnDangXuat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDangXuat.Location = new System.Drawing.Point(725, 45);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(131, 53);
            this.btnDangXuat.TabIndex = 50;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDangXuat.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Nha_sach_506.Properties.Resources.banner_bookshelf;
            this.pictureBox1.Location = new System.Drawing.Point(1, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(881, 141);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // frmThuNgan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 733);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.btnDangXuat);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.lblChucVu);
            this.Controls.Add(this.lblTenNguoiDung);
            this.Controls.Add(this.tabNhaSach);
            this.Controls.Add(this.pictureBox1);
            this.Name = "frmThuNgan";
            this.Text = "Phần mềm quản lý Nhà sách 506 - Phiên bản 2";
            this.Load += new System.EventHandler(this.giao_dien_thu_ngan_Load);
            this.tabNhaSach.ResumeLayout(false);
            this.tabTrangChu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).EndInit();
            this.tabTraCuu.ResumeLayout(false);
            this.tabTraCuu.PerformLayout();
            this.grpKhachHang_tc.ResumeLayout(false);
            this.grpKhachHang_tc.PerformLayout();
            this.grpHoaDon_tc.ResumeLayout(false);
            this.grpHoaDon_tc.PerformLayout();
            this.grpSach_tc.ResumeLayout(false);
            this.grpSach_tc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTraCuu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).EndInit();
            this.tabHoaDon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHoaDon)).EndInit();
            this.grpNhapHoaDon.ResumeLayout(false);
            this.grpNhapHoaDon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoLuong_hd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNhapHoaDon)).EndInit();
            this.grpThanhToan.ResumeLayout(false);
            this.grpThanhToan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picThanhToan)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picDkkhtt)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picKhNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDate;
        internal System.Windows.Forms.Button btnDangXuat;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label lblChucVu;
        internal System.Windows.Forms.Label lblTenNguoiDung;
        private System.Windows.Forms.TabControl tabNhaSach;
        private System.Windows.Forms.TabPage tabTrangChu;
        private System.Windows.Forms.PictureBox picTrangChu;
        private System.Windows.Forms.TabPage tabTraCuu;
        private System.Windows.Forms.GroupBox grpKhachHang_tc;
        private System.Windows.Forms.GroupBox grpHoaDon_tc;
        private System.Windows.Forms.GroupBox grpSach_tc;
        internal System.Windows.Forms.CheckBox chkConHang;
        internal System.Windows.Forms.DataGridView dgvTraCuu;
        internal System.Windows.Forms.TextBox txtTuKhoa;
        internal System.Windows.Forms.Label lblTuKhoa;
        internal System.Windows.Forms.PictureBox picTraCuu;
        internal System.Windows.Forms.PictureBox PictureBox4;
        private System.Windows.Forms.TabPage tabHoaDon;
        internal System.Windows.Forms.DataGridView dgvHoaDon;
        internal System.Windows.Forms.GroupBox grpNhapHoaDon;
        private System.Windows.Forms.Label lblDate_hd;
        internal System.Windows.Forms.ComboBox cboTenSach_hd;
        internal System.Windows.Forms.Label lblNgayThang_hd;
        internal System.Windows.Forms.NumericUpDown numSoLuong_hd;
        internal System.Windows.Forms.Label lblSoLuong_hd;
        internal System.Windows.Forms.Label lblTenSach_hd;
        internal System.Windows.Forms.PictureBox picNhapHoaDon;
        internal System.Windows.Forms.GroupBox grpThanhToan;
        internal System.Windows.Forms.Button btnDangKyNo;
        internal System.Windows.Forms.Label lblDong_tt;
        internal System.Windows.Forms.Label lblDong_tn;
        internal System.Windows.Forms.Label lblDong_tc;
        internal System.Windows.Forms.Label lblTienThoi;
        internal System.Windows.Forms.TextBox txtTienThoi_hd;
        internal System.Windows.Forms.TextBox txtTienNhan_hd;
        internal System.Windows.Forms.Label lblTienNhan_hd;
        internal System.Windows.Forms.TextBox txtTongCong_hd;
        internal System.Windows.Forms.Label lblTongCong_hd;
        internal System.Windows.Forms.PictureBox picThanhToan;
        internal System.Windows.Forms.Button btnXuatHoaDon_hd;
        internal System.Windows.Forms.Button btnTinhTien_hd;
        internal System.Windows.Forms.Button btnXoaSach_hd;
        internal System.Windows.Forms.Button btnXoa_hd;
        internal System.Windows.Forms.Button btnThem_hd;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button btnHuyBo_kh;
        internal System.Windows.Forms.Button btnDangKy_kh;
        internal System.Windows.Forms.TextBox txtEmail;
        internal System.Windows.Forms.Label lblEmail;
        internal System.Windows.Forms.TextBox txtDienThoai;
        internal System.Windows.Forms.TextBox txtDiaChi;
        internal System.Windows.Forms.Label lblDienThoai;
        internal System.Windows.Forms.Label lblDiaChi;
        internal System.Windows.Forms.TextBox txtMaKhachHang;
        internal System.Windows.Forms.Label lblMaKhachHang;
        internal System.Windows.Forms.TextBox txtHoTen;
        internal System.Windows.Forms.Label lblHoTen;
        internal System.Windows.Forms.Label lblDkkhtt;
        internal System.Windows.Forms.PictureBox picDkkhtt;
        private System.Windows.Forms.RadioButton rbtHD_tc;
        internal System.Windows.Forms.CheckBox chkKhachNo;
        private System.Windows.Forms.RadioButton rbtKH_tc;
        private System.Windows.Forms.RadioButton rbtS_tc;
        private System.Windows.Forms.TabPage tabPage2;
        internal System.Windows.Forms.TextBox txtKey;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDate_ttno;
        internal System.Windows.Forms.Button btnXoaNo;
        internal System.Windows.Forms.Label lblThoiGian_khn;
        private System.Windows.Forms.DataGridView dataGridView1;
        internal System.Windows.Forms.PictureBox picKhNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTienNo;
    }
}