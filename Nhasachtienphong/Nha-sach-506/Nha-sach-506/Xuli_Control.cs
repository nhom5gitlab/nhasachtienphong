﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace Nha_sach_506
{
    class Xuli_Control
    {

        public int nhapsach(string masach,string tensach,string tacgia,string nxb,string theloai,int soluong,int dongia,string ngay)
        {
              KetnoiSQL kn = new KetnoiSQL();
              // Database nhasach506, table sach
              return kn.xulydulieu("use nhasach506 Insert into SACH(masach,tensach,tacgia,nxb,theloai,soluong,dongia,ngaynhap) values('"+masach+"',N'"+tensach+"',N'"+tacgia+"',N'"+nxb+"',N'"+theloai+"','"+soluong+"','"+dongia+"','"+ngay+"')");

             

           

        }

        // Hàm lấy giá trị cao nhất trong mã sách.....
        public int laymasach()
        {
            KetnoiSQL kn = new KetnoiSQL();
            DataTable dt = new DataTable();
         
            dt.Columns.Add("MASACH");

            // Database nhasach506, table sach, trường masach
            dt = kn.laybang("use nhasach506 select MASACH from SACH");
            string str = "";
            //Nếu không có dữ liệu trong database thì trả về 0
            if(dt.Rows.Count<=0) return 0;
            foreach (DataRow r in dt.Rows)
            {
                str = r["MASACH"].ToString();
                r["MASACH"] = int.Parse(str.Substring(2).ToString());
            }
            int max=0;
            //tìm và trả về mã sách max
            foreach (DataRow r in dt.Rows)
            {
                if (int.Parse(r["MASACH"].ToString()) > max)
                    max = int.Parse(r["MASACH"].ToString());
            }
            return max;

        }
        // Hàm ghép mã vừa tìm được vào MS00000-
        public string nhapmasach()
        {
            int ma=laymasach();
            if (ma == 0)
                return "MS0000001";
            else
            {                
                int ma4 = ma + 1;
                return ("MS" + ma4.ToString("0000000"));

            }
        }


        public int xettensach(string str)
        {
            KetnoiSQL kn = new KetnoiSQL();
            DataTable dt = new DataTable();
            dt.Columns.Add("TENSACH");
            dt = kn.laybang("use nhasach506 select TENSACH from SACH");
            foreach (DataRow r in dt.Rows)
            {//hàm String.Compare dùng để so sánh hai chuổi, trả về 0 nếu hai chuổi giống nhau
                if (String.Compare(r["TENSACH"].ToString(), str) == 0)
                    return 1;
            }
            return 0;
        }
        public string timmatheoten(string str)
        {
            KetnoiSQL kn = new KetnoiSQL();
            return kn.lay1giatri("use nhasach506 select MASACH as [tong] from SACH where TENSACH like N'" + str + "' ");
        }

        public int capnhatsachdaco(string ma,int sl,int dg)
        {
            KetnoiSQL kn = new KetnoiSQL();
            // Database nhasach506, table sach
            return kn.xulydulieu("use nhasach506 update SACH set SOLUONG='"+sl+"',DONGIA='"+dg+"' where MASACH ='"+ma+"'");
        }

        public void laydstensach(TextBox tbtensach,TextBox tbtacgia,TextBox tbNXB,TextBox tbdongia)
        {            
            KetnoiSQL kn = new KetnoiSQL();
            DataTable dt = new DataTable();
            dt.Columns.Add("TENSACH");
            dt.Columns.Add("TACGIA");
            //dt.Columns.Add("THELOAI");
            dt.Columns.Add("NXB");
            dt.Columns.Add("DONGIA");
            dt = kn.laybang("use nhasach506 select TENSACH,TACGIA,THELOAI,NXB,DONGIA from SACH");
            //tbtensach.DataBindings.Add("text",dt,"TENSACH", true);
            tbtensach.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            tbtacgia.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //cbtheloai.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            tbNXB.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            tbdongia.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            tbtensach.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tbtacgia.AutoCompleteSource = AutoCompleteSource.CustomSource;
            //cbtheloai.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tbNXB.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tbdongia.AutoCompleteSource = AutoCompleteSource.CustomSource;

            AutoCompleteStringCollection auto1 = new AutoCompleteStringCollection();
            AutoCompleteStringCollection auto2 = new AutoCompleteStringCollection();
            AutoCompleteStringCollection auto3 = new AutoCompleteStringCollection();
            AutoCompleteStringCollection auto4 = new AutoCompleteStringCollection();
            AutoCompleteStringCollection auto5 = new AutoCompleteStringCollection();

            foreach (DataRow r in dt.Rows)
            {
               auto1.Add(r["TENSACH"].ToString());
               auto2.Add(r["TACGIA"].ToString());
               //auto3.Add(r["THELOAI"].ToString());
               auto4.Add(r["NXB"].ToString());
               auto5.Add(r["DONGIA"].ToString());
            }
            tbtensach.AutoCompleteCustomSource = auto1;
            tbtacgia.AutoCompleteCustomSource = auto2;
            //cbtheloai.AutoCompleteCustomSource = auto3;
            tbNXB.AutoCompleteCustomSource = auto4;
            tbdongia.AutoCompleteCustomSource = auto5;


        }

    }
}
