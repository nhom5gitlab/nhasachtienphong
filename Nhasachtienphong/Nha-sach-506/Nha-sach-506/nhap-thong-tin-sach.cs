﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
namespace Nha_sach_506
{
    public partial class frmNhapSach : Form
    {
        public frmNhapSach()
        {
            InitializeComponent();
        }

        private void frmNhapSach_Load(object sender, EventArgs e)
        {
            lblDate_ns.Text = DateTime.Now.ToShortDateString();
            Xuli_Control xl = new Xuli_Control();
            //dgvNhapSach.Columns.Add("masach","Mã sách");
            dgvNhapSach.Columns.Add("tensach","Tên sách");
            dgvNhapSach.Columns.Add("tacgia","Tác giả");
            dgvNhapSach.Columns.Add("namxuatban","Năm xuất bản");
            dgvNhapSach.Columns.Add("theloai","Thể loại");
            dgvNhapSach.Columns.Add("soluong","Số lượng");
            dgvNhapSach.Columns.Add("dongia","Đơn giá");
            dgvNhapSach.Columns.Add("ngaynhap","Ngày nhập");
            lbNgaynhap.Text = DateTime.Now.ToString();
            xl.laydstensach(txtTenSach_ns,txtTacGia_ns,txtNamXuatBan_ns,txtDonGia_ns);
            xulian();
            btnNhapSach_ns.Enabled = false;

            

        }


        //DataTable Dttb_ns = new DataTable();
        //Dttb_ns.Columns.Add("masach");
        //Dttb_ns.Columns.Add("tensach");
        //Dttb_ns.Columns.Add("tacgia");
        //Dttb_ns.Columns.Add("namxuatban");
        //Dttb_ns.Columns.Add("theloai");
        //Dttb_ns.Columns.Add("soluong");
        //Dttb_ns.Columns.Add("dongia");
        //Dttb_ns.Columns.Add("ngaynhap");



        public int hangchon = 0;

        private void dgvNhapSach_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int dong;
                dong = e.RowIndex;

                //this.txtMaSach_ns.Text = dgvNhapSach.Rows[dong].Cells[0].Value.ToString();
                this.txtTenSach_ns.Text = dgvNhapSach.Rows[dong].Cells[0].Value.ToString();                
                this.txtTacGia_ns.Text = dgvNhapSach.Rows[dong].Cells[1].Value.ToString();
                this.txtNamXuatBan_ns.Text = dgvNhapSach.Rows[dong].Cells[2].Value.ToString();                
                this.cboTheLoai_ns.Text = dgvNhapSach.Rows[dong].Cells[3].Value.ToString();
                this.txtSoLuong_ns.Text = dgvNhapSach.Rows[dong].Cells[4].Value.ToString();
                this.txtDonGia_ns.Text = dgvNhapSach.Rows[dong].Cells[5].Value.ToString();
                hangchon = dong;
                btnSua_ns.Enabled = true;
                btnXoa_ns.Enabled = true;
            }
            catch
            {
            }
        }

        public void xulian()
        {
            if (txtTenSach_ns.Text == "" || txtTacGia_ns.Text == "" || txtNamXuatBan_ns.Text == "" || txtSoLuong_ns.Text == "" || txtDonGia_ns.Text == "" || cboTheLoai_ns.Text == "")
            {
                btnThem_ns.Enabled = false;
                //btnNhapSach_ns.Enabled = false;
                btnSua_ns.Enabled = false;
                btnXoa_ns.Enabled = false;

            }
            else
            {
                btnThem_ns.Enabled = true;
            }
            if (dgvNhapSach.RowCount <= 0)
                btnNhapSach_ns.Enabled = false;
            else
                btnNhapSach_ns.Enabled = true;
            


        }

        private void btnThem_ns_Click(object sender, EventArgs e)
        {
            dgvNhapSach.Rows.Add(txtTenSach_ns.Text, txtTacGia_ns.Text, txtNamXuatBan_ns.Text, cboTheLoai_ns.ValueMember.ToString(), txtSoLuong_ns.Text, txtDonGia_ns.Text,DateTime.Now.ToString());

            txtTenSach_ns.Text = "";
            txtTacGia_ns.Text = "";
            txtSoLuong_ns.Text = "";
            txtDonGia_ns.Text = "";
            txtNamXuatBan_ns.Text = "";

            btnNhapSach_ns.Enabled = true;
            btnThem_ns.Enabled = false;

        }

        private void btnNhapSach_ns_Click(object sender, EventArgs e)
        {
            Xuli_Control xl = new Xuli_Control();
            int kq = 0;
            try
            {
                //MessageBox.Show("OK");

                for (int i = 0; i < dgvNhapSach.RowCount; i++)
                {


                    //string ms = dgvNhapSach.Rows[i].Cells[0].Value.ToString();
                    string ten = dgvNhapSach.Rows[i].Cells[0].Value.ToString();
                    string tg = dgvNhapSach.Rows[i].Cells[1].Value.ToString();
                    string nxb = dgvNhapSach.Rows[i].Cells[2].Value.ToString();
                    string theloai = dgvNhapSach.Rows[i].Cells[3].Value.ToString();
                    int soluong = int.Parse(dgvNhapSach.Rows[i].Cells[4].Value.ToString());
                    int dongia = int.Parse(dgvNhapSach.Rows[i].Cells[5].Value.ToString());
                    string ngay = dgvNhapSach.Rows[i].Cells[6].Value.ToString();
                    string ms = "0";
                    if (xl.xettensach(ten) == 1)
                    {
                        ms = xl.timmatheoten(ten);
                        kq = xl.capnhatsachdaco(ms, soluong, dongia);
                        if (kq > 0)
                        {
                            MessageBox.Show("Lưu dữ liệu thành công");
                        }
                        else
                        {
                            MessageBox.Show("Lưu dữ liệu không thành công");
                        }

                    }
                    else
                    {
                        ms = xl.nhapmasach();


                        kq = xl.nhapsach(ms, ten, tg, nxb, theloai, soluong, dongia, ngay);
                        if (kq > 0)
                        {
                            MessageBox.Show("Lưu dữ liệu thành công");
                        }
                        else
                        {
                            MessageBox.Show("Lưu dữ liệu không thành công");
                        }
                    }
                }

                

            }
            catch (Exception)
            {
            }
            
            


        }

        private void btnXoa_ns_Click(object sender, EventArgs e)
        {

            if (this.dgvNhapSach.SelectedRows.Count > 0)
            { dgvNhapSach.Rows.RemoveAt(this.dgvNhapSach.SelectedRows[0].Index); }

            xulian();
            txtTenSach_ns.Text = "";
            txtTacGia_ns.Text = "";
            txtSoLuong_ns.Text = "";
            txtDonGia_ns.Text = "";
            txtNamXuatBan_ns.Text = "";
        }

        private void btnSua_ns_Click(object sender, EventArgs e)
        {
            //dgvNhapSach.Rows[hangchon].Cells[0].Value = txtMaSach_ns.Text;
            dgvNhapSach.Rows[hangchon].Cells[0].Value = txtTenSach_ns.Text;
            dgvNhapSach.Rows[hangchon].Cells[1].Value = txtTacGia_ns.Text;
            dgvNhapSach.Rows[hangchon].Cells[2].Value = txtNamXuatBan_ns.Text;
            dgvNhapSach.Rows[hangchon].Cells[3].Value = cboTheLoai_ns.ValueMember.ToString();
            dgvNhapSach.Rows[hangchon].Cells[4].Value = txtSoLuong_ns.Text;
            dgvNhapSach.Rows[hangchon].Cells[5].Value = txtDonGia_ns.Text;
            

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnHuyBo_ns_Click(object sender, EventArgs e)
        {
            this.Close();
        }




        private void txtSoLuong_ns_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtDonGia_ns_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void txtTenSach_ns_TextChanged(object sender, EventArgs e)
        {
            xulian();
        }

        private void txtTacGia_ns_TextChanged(object sender, EventArgs e)
        {
            xulian();
        }

        private void txtNamXuatBan_ns_TextChanged(object sender, EventArgs e)
        {
            xulian();
        }

        private void txtSoLuong_ns_TextChanged(object sender, EventArgs e)
        {
            xulian();
        }

        private void txtDonGia_ns_TextChanged(object sender, EventArgs e)
        {
            xulian();
        }

        private void cboTheLoai_ns_SelectedIndexChanged(object sender, EventArgs e)
        {
            xulian();
        }


        
    }
}
