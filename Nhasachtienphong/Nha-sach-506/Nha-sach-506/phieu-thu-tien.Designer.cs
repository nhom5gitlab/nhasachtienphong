﻿namespace Nha_sach_506
{
    partial class frmPhieuThuTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPhieuThuTien = new System.Windows.Forms.DataGridView();
            this.lblThoiGian_ptt = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.picPhieuThuTien = new System.Windows.Forms.PictureBox();
            this.lblDate_ptt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhieuThuTien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhieuThuTien)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPhieuThuTien
            // 
            this.dgvPhieuThuTien.BackgroundColor = System.Drawing.Color.Beige;
            this.dgvPhieuThuTien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPhieuThuTien.Location = new System.Drawing.Point(12, 252);
            this.dgvPhieuThuTien.Name = "dgvPhieuThuTien";
            this.dgvPhieuThuTien.Size = new System.Drawing.Size(582, 189);
            this.dgvPhieuThuTien.TabIndex = 51;
            // 
            // lblThoiGian_ptt
            // 
            this.lblThoiGian_ptt.AutoSize = true;
            this.lblThoiGian_ptt.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoiGian_ptt.Location = new System.Drawing.Point(22, 225);
            this.lblThoiGian_ptt.Name = "lblThoiGian_ptt";
            this.lblThoiGian_ptt.Size = new System.Drawing.Size(45, 17);
            this.lblThoiGian_ptt.TabIndex = 50;
            this.lblThoiGian_ptt.Text = "Ngày:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label1.Location = new System.Drawing.Point(231, 136);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(245, 34);
            this.Label1.TabIndex = 49;
            this.Label1.Text = "PHIẾU THU TIỀN";
            // 
            // picPhieuThuTien
            // 
            this.picPhieuThuTien.Image = global::Nha_sach_506.Properties.Resources._506_LOGO1;
            this.picPhieuThuTien.Location = new System.Drawing.Point(54, 12);
            this.picPhieuThuTien.Name = "picPhieuThuTien";
            this.picPhieuThuTien.Size = new System.Drawing.Size(472, 169);
            this.picPhieuThuTien.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPhieuThuTien.TabIndex = 48;
            this.picPhieuThuTien.TabStop = false;
            // 
            // lblDate_ptt
            // 
            this.lblDate_ptt.AutoSize = true;
            this.lblDate_ptt.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate_ptt.Location = new System.Drawing.Point(62, 223);
            this.lblDate_ptt.Name = "lblDate_ptt";
            this.lblDate_ptt.Size = new System.Drawing.Size(45, 20);
            this.lblDate_ptt.TabIndex = 52;
            this.lblDate_ptt.Text = "label2";
            // 
            // frmPhieuThuTien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 477);
            this.Controls.Add(this.lblDate_ptt);
            this.Controls.Add(this.dgvPhieuThuTien);
            this.Controls.Add(this.lblThoiGian_ptt);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.picPhieuThuTien);
            this.Name = "frmPhieuThuTien";
            this.Text = "Phiếu thu tiền";
            this.Load += new System.EventHandler(this.frmPhieuThuTien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhieuThuTien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPhieuThuTien)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridView dgvPhieuThuTien;
        internal System.Windows.Forms.Label lblThoiGian_ptt;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.PictureBox picPhieuThuTien;
        private System.Windows.Forms.Label lblDate_ptt;
    }
}