﻿namespace Nha_sach_506
{
    partial class frmDangKiNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDangKiNo));
            this.picKhNo = new System.Windows.Forms.PictureBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtMaKhachHang_dkn = new System.Windows.Forms.TextBox();
            this.lblMaKhachHang = new System.Windows.Forms.Label();
            this.txtHoTen_dkn = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDangKyNo_dkn = new System.Windows.Forms.Button();
            this.txtSoTien_dkn = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picKhNo)).BeginInit();
            this.SuspendLayout();
            // 
            // picKhNo
            // 
            this.picKhNo.Image = ((System.Drawing.Image)(resources.GetObject("picKhNo.Image")));
            this.picKhNo.Location = new System.Drawing.Point(55, 12);
            this.picKhNo.Name = "picKhNo";
            this.picKhNo.Size = new System.Drawing.Size(472, 169);
            this.picKhNo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picKhNo.TabIndex = 52;
            this.picKhNo.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.Label1.Location = new System.Drawing.Point(273, 133);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(202, 34);
            this.Label1.TabIndex = 53;
            this.Label1.Text = "ĐĂNG KÝ NỢ";
            // 
            // txtMaKhachHang_dkn
            // 
            this.txtMaKhachHang_dkn.Enabled = false;
            this.txtMaKhachHang_dkn.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaKhachHang_dkn.Location = new System.Drawing.Point(283, 208);
            this.txtMaKhachHang_dkn.Name = "txtMaKhachHang_dkn";
            this.txtMaKhachHang_dkn.Size = new System.Drawing.Size(192, 25);
            this.txtMaKhachHang_dkn.TabIndex = 55;
            // 
            // lblMaKhachHang
            // 
            this.lblMaKhachHang.AutoSize = true;
            this.lblMaKhachHang.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaKhachHang.Location = new System.Drawing.Point(168, 213);
            this.lblMaKhachHang.Name = "lblMaKhachHang";
            this.lblMaKhachHang.Size = new System.Drawing.Size(109, 17);
            this.lblMaKhachHang.TabIndex = 54;
            this.lblMaKhachHang.Text = "Mã khách hàng";
            // 
            // txtHoTen_dkn
            // 
            this.txtHoTen_dkn.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTen_dkn.Location = new System.Drawing.Point(283, 248);
            this.txtHoTen_dkn.Name = "txtHoTen_dkn";
            this.txtHoTen_dkn.Size = new System.Drawing.Size(192, 25);
            this.txtHoTen_dkn.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(168, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 58;
            this.label7.Text = "Họ và Tên :";
            // 
            // btnDangKyNo_dkn
            // 
            this.btnDangKyNo_dkn.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnDangKyNo_dkn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangKyNo_dkn.Image = global::Nha_sach_506.Properties.Resources.save_48x48;
            this.btnDangKyNo_dkn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDangKyNo_dkn.Location = new System.Drawing.Point(450, 342);
            this.btnDangKyNo_dkn.Name = "btnDangKyNo_dkn";
            this.btnDangKyNo_dkn.Size = new System.Drawing.Size(162, 57);
            this.btnDangKyNo_dkn.TabIndex = 60;
            this.btnDangKyNo_dkn.Text = "Đăng ký nợ";
            this.btnDangKyNo_dkn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDangKyNo_dkn.UseVisualStyleBackColor = false;
            // 
            // txtSoTien_dkn
            // 
            this.txtSoTien_dkn.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTien_dkn.Location = new System.Drawing.Point(283, 290);
            this.txtSoTien_dkn.Name = "txtSoTien_dkn";
            this.txtSoTien_dkn.Size = new System.Drawing.Size(192, 25);
            this.txtSoTien_dkn.TabIndex = 62;
            this.txtSoTien_dkn.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(168, 295);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 61;
            this.label2.Text = "Số tiền nợ :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // frmDangKiNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 424);
            this.Controls.Add(this.txtSoTien_dkn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDangKyNo_dkn);
            this.Controls.Add(this.txtHoTen_dkn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtMaKhachHang_dkn);
            this.Controls.Add(this.lblMaKhachHang);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.picKhNo);
            this.Name = "frmDangKiNo";
            this.Text = "dang_ky_no";
            ((System.ComponentModel.ISupportInitialize)(this.picKhNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox picKhNo;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtMaKhachHang_dkn;
        internal System.Windows.Forms.Label lblMaKhachHang;
        internal System.Windows.Forms.TextBox txtHoTen_dkn;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Button btnDangKyNo_dkn;
        internal System.Windows.Forms.TextBox txtSoTien_dkn;
        internal System.Windows.Forms.Label label2;
    }
}