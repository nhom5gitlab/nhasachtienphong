﻿namespace Nha_sach_506
{
    partial class frmBaoCaoNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label2 = new System.Windows.Forms.Label();
            this.btnThoat_bcn = new System.Windows.Forms.Button();
            this.txtNam_bcn = new System.Windows.Forms.TextBox();
            this.lblNam_bcn = new System.Windows.Forms.Label();
            this.txtThang_bcn = new System.Windows.Forms.TextBox();
            this.lblThang_bcn = new System.Windows.Forms.Label();
            this.dgvBaoCaoNo = new System.Windows.Forms.DataGridView();
            this.Label1 = new System.Windows.Forms.Label();
            this.picBaoCaoNo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBaoCaoNo)).BeginInit();
            this.SuspendLayout();
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(501, 548);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(266, 17);
            this.Label2.TabIndex = 41;
            this.Label2.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // btnThoat_bcn
            // 
            this.btnThoat_bcn.BackColor = System.Drawing.Color.Red;
            this.btnThoat_bcn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThoat_bcn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnThoat_bcn.Location = new System.Drawing.Point(604, 224);
            this.btnThoat_bcn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnThoat_bcn.Name = "btnThoat_bcn";
            this.btnThoat_bcn.Size = new System.Drawing.Size(192, 57);
            this.btnThoat_bcn.TabIndex = 40;
            this.btnThoat_bcn.Text = "Thoát";
            this.btnThoat_bcn.UseVisualStyleBackColor = false;
            // 
            // txtNam_bcn
            // 
            this.txtNam_bcn.Location = new System.Drawing.Point(315, 256);
            this.txtNam_bcn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNam_bcn.Name = "txtNam_bcn";
            this.txtNam_bcn.Size = new System.Drawing.Size(140, 22);
            this.txtNam_bcn.TabIndex = 39;
            // 
            // lblNam_bcn
            // 
            this.lblNam_bcn.AutoSize = true;
            this.lblNam_bcn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNam_bcn.Location = new System.Drawing.Point(252, 255);
            this.lblNam_bcn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNam_bcn.Name = "lblNam_bcn";
            this.lblNam_bcn.Size = new System.Drawing.Size(50, 23);
            this.lblNam_bcn.TabIndex = 38;
            this.lblNam_bcn.Text = "Năm";
            // 
            // txtThang_bcn
            // 
            this.txtThang_bcn.Location = new System.Drawing.Point(87, 256);
            this.txtThang_bcn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtThang_bcn.Name = "txtThang_bcn";
            this.txtThang_bcn.Size = new System.Drawing.Size(139, 22);
            this.txtThang_bcn.TabIndex = 37;
            // 
            // lblThang_bcn
            // 
            this.lblThang_bcn.AutoSize = true;
            this.lblThang_bcn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThang_bcn.Location = new System.Drawing.Point(11, 255);
            this.lblThang_bcn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThang_bcn.Name = "lblThang_bcn";
            this.lblThang_bcn.Size = new System.Drawing.Size(64, 23);
            this.lblThang_bcn.TabIndex = 36;
            this.lblThang_bcn.Text = "Tháng";
            // 
            // dgvBaoCaoNo
            // 
            this.dgvBaoCaoNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBaoCaoNo.Location = new System.Drawing.Point(17, 297);
            this.dgvBaoCaoNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBaoCaoNo.Name = "dgvBaoCaoNo";
            this.dgvBaoCaoNo.RowHeadersWidth = 51;
            this.dgvBaoCaoNo.Size = new System.Drawing.Size(779, 244);
            this.dgvBaoCaoNo.TabIndex = 35;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Label1.Location = new System.Drawing.Point(248, 184);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(283, 46);
            this.Label1.TabIndex = 33;
            this.Label1.Text = "BÁO CÁO NỢ";
            // 
            // picBaoCaoNo
            // 
            this.picBaoCaoNo.Image = global::Nha_sach_506.Properties.Resources.nhasach;
            this.picBaoCaoNo.Location = new System.Drawing.Point(131, 11);
            this.picBaoCaoNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.picBaoCaoNo.Name = "picBaoCaoNo";
            this.picBaoCaoNo.Size = new System.Drawing.Size(549, 191);
            this.picBaoCaoNo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBaoCaoNo.TabIndex = 34;
            this.picBaoCaoNo.TabStop = false;
            // 
            // frmBaoCaoNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 576);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.btnThoat_bcn);
            this.Controls.Add(this.txtNam_bcn);
            this.Controls.Add(this.lblNam_bcn);
            this.Controls.Add(this.txtThang_bcn);
            this.Controls.Add(this.lblThang_bcn);
            this.Controls.Add(this.dgvBaoCaoNo);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.picBaoCaoNo);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmBaoCaoNo";
            this.Text = "Báo cáo nợ";
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBaoCaoNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button btnThoat_bcn;
        internal System.Windows.Forms.TextBox txtNam_bcn;
        internal System.Windows.Forms.Label lblNam_bcn;
        internal System.Windows.Forms.TextBox txtThang_bcn;
        internal System.Windows.Forms.Label lblThang_bcn;
        internal System.Windows.Forms.DataGridView dgvBaoCaoNo;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.PictureBox picBaoCaoNo;

    }
}