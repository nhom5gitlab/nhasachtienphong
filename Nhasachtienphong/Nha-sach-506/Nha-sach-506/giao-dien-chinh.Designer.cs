﻿namespace Nha_sach_506
{
    partial class frmNhaSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabNhaSach = new System.Windows.Forms.TabControl();
            this.tabTrangChu = new System.Windows.Forms.TabPage();
            this.picTrangChu = new System.Windows.Forms.PictureBox();
            this.tabQuanLySach = new System.Windows.Forms.TabPage();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.cboTheLoai_qls = new System.Windows.Forms.ComboBox();
            this.lblTheLoai_qls = new System.Windows.Forms.Label();
            this.lblMaSach_qls = new System.Windows.Forms.Label();
            this.lblDonGia_qls = new System.Windows.Forms.Label();
            this.txtDonGia_qls = new System.Windows.Forms.TextBox();
            this.txtMaSach_qls = new System.Windows.Forms.TextBox();
            this.lblSoLuong_qls = new System.Windows.Forms.Label();
            this.txtTenSach_qls = new System.Windows.Forms.TextBox();
            this.lblTenSach_qls = new System.Windows.Forms.Label();
            this.lblNamXuatBan_qls = new System.Windows.Forms.Label();
            this.txtTacGia_qls = new System.Windows.Forms.TextBox();
            this.txtNamXuatBan_qls = new System.Windows.Forms.TextBox();
            this.lblTacGia_qls = new System.Windows.Forms.Label();
            this.dgvQuanLySach = new System.Windows.Forms.DataGridView();
            this.btnNhapSach_qls = new System.Windows.Forms.Button();
            this.btnHuyBo_qls = new System.Windows.Forms.Button();
            this.btnLuu_qls = new System.Windows.Forms.Button();
            this.btnXoa_qls = new System.Windows.Forms.Button();
            this.picQuanLySach = new System.Windows.Forms.PictureBox();
            this.btnSua_qls = new System.Windows.Forms.Button();
            this.tabTraCuu = new System.Windows.Forms.TabPage();
            this.grpKhachHang_tc = new System.Windows.Forms.GroupBox();
            this.rbtKH_tc = new System.Windows.Forms.RadioButton();
            this.chkKhachNo = new System.Windows.Forms.CheckBox();
            this.grpHoaDon_tc = new System.Windows.Forms.GroupBox();
            this.rbtHD_tc = new System.Windows.Forms.RadioButton();
            this.grpSach_tc = new System.Windows.Forms.GroupBox();
            this.rbtS_tc = new System.Windows.Forms.RadioButton();
            this.chkConHang = new System.Windows.Forms.CheckBox();
            this.dgvTraCuu = new System.Windows.Forms.DataGridView();
            this.txtTuKhoa = new System.Windows.Forms.TextBox();
            this.lblTuKhoa = new System.Windows.Forms.Label();
            this.picTraCuu = new System.Windows.Forms.PictureBox();
            this.PictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabTruyVanDuLieu = new System.Windows.Forms.TabPage();
            this.txtNam_tvdl = new System.Windows.Forms.TextBox();
            this.lblNam = new System.Windows.Forms.Label();
            this.txtThang_tvdl = new System.Windows.Forms.TextBox();
            this.lblThang = new System.Windows.Forms.Label();
            this.btnBaoCaoNo = new System.Windows.Forms.Button();
            this.btnBaoCaoTon = new System.Windows.Forms.Button();
            this.picTruyVanDuLieu = new System.Windows.Forms.PictureBox();
            this.tabQuyDinh = new System.Windows.Forms.TabPage();
            this.rbtNo_qd = new System.Windows.Forms.RadioButton();
            this.rbtYes_qd = new System.Windows.Forms.RadioButton();
            this.txtTonMinSauBan_qd = new System.Windows.Forms.TextBox();
            this.txtNoMax_qd = new System.Windows.Forms.TextBox();
            this.txtTonMin_qd = new System.Windows.Forms.TextBox();
            this.txtNhapMin_qd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblQuydinh = new System.Windows.Forms.Label();
            this.btnApDung_qd = new System.Windows.Forms.Button();
            this.btnHuyBo_qd = new System.Windows.Forms.Button();
            this.btnSua_qd = new System.Windows.Forms.Button();
            this.lblChucVu = new System.Windows.Forms.Label();
            this.lblTenNguoiDung = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabNhaSach.SuspendLayout();
            this.tabTrangChu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).BeginInit();
            this.tabQuanLySach.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuanLySach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuanLySach)).BeginInit();
            this.tabTraCuu.SuspendLayout();
            this.grpKhachHang_tc.SuspendLayout();
            this.grpHoaDon_tc.SuspendLayout();
            this.grpSach_tc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTraCuu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).BeginInit();
            this.tabTruyVanDuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTruyVanDuLieu)).BeginInit();
            this.tabQuyDinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabNhaSach
            // 
            this.tabNhaSach.Controls.Add(this.tabTrangChu);
            this.tabNhaSach.Controls.Add(this.tabQuanLySach);
            this.tabNhaSach.Controls.Add(this.tabTraCuu);
            this.tabNhaSach.Controls.Add(this.tabTruyVanDuLieu);
            this.tabNhaSach.Controls.Add(this.tabQuyDinh);
            this.tabNhaSach.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabNhaSach.Location = new System.Drawing.Point(3, 183);
            this.tabNhaSach.Margin = new System.Windows.Forms.Padding(4);
            this.tabNhaSach.Name = "tabNhaSach";
            this.tabNhaSach.SelectedIndex = 0;
            this.tabNhaSach.Size = new System.Drawing.Size(1175, 651);
            this.tabNhaSach.TabIndex = 1;
            // 
            // tabTrangChu
            // 
            this.tabTrangChu.Controls.Add(this.picTrangChu);
            this.tabTrangChu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabTrangChu.Location = new System.Drawing.Point(4, 33);
            this.tabTrangChu.Margin = new System.Windows.Forms.Padding(4);
            this.tabTrangChu.Name = "tabTrangChu";
            this.tabTrangChu.Padding = new System.Windows.Forms.Padding(4);
            this.tabTrangChu.Size = new System.Drawing.Size(1167, 614);
            this.tabTrangChu.TabIndex = 0;
            this.tabTrangChu.Text = "Trang chủ";
            this.tabTrangChu.UseVisualStyleBackColor = true;
            // 
            // picTrangChu
            // 
            this.picTrangChu.Image = global::Nha_sach_506.Properties.Resources.check_yes_ok_icone_7166_48;
            this.picTrangChu.Location = new System.Drawing.Point(161, 136);
            this.picTrangChu.Margin = new System.Windows.Forms.Padding(4);
            this.picTrangChu.Name = "picTrangChu";
            this.picTrangChu.Size = new System.Drawing.Size(877, 302);
            this.picTrangChu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTrangChu.TabIndex = 0;
            this.picTrangChu.TabStop = false;
            this.picTrangChu.Click += new System.EventHandler(this.picTrangChu_Click);
            // 
            // tabQuanLySach
            // 
            this.tabQuanLySach.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabQuanLySach.Controls.Add(this.numericUpDown1);
            this.tabQuanLySach.Controls.Add(this.cboTheLoai_qls);
            this.tabQuanLySach.Controls.Add(this.lblTheLoai_qls);
            this.tabQuanLySach.Controls.Add(this.lblMaSach_qls);
            this.tabQuanLySach.Controls.Add(this.lblDonGia_qls);
            this.tabQuanLySach.Controls.Add(this.txtDonGia_qls);
            this.tabQuanLySach.Controls.Add(this.txtMaSach_qls);
            this.tabQuanLySach.Controls.Add(this.lblSoLuong_qls);
            this.tabQuanLySach.Controls.Add(this.txtTenSach_qls);
            this.tabQuanLySach.Controls.Add(this.lblTenSach_qls);
            this.tabQuanLySach.Controls.Add(this.lblNamXuatBan_qls);
            this.tabQuanLySach.Controls.Add(this.txtTacGia_qls);
            this.tabQuanLySach.Controls.Add(this.txtNamXuatBan_qls);
            this.tabQuanLySach.Controls.Add(this.lblTacGia_qls);
            this.tabQuanLySach.Controls.Add(this.dgvQuanLySach);
            this.tabQuanLySach.Controls.Add(this.btnNhapSach_qls);
            this.tabQuanLySach.Controls.Add(this.btnHuyBo_qls);
            this.tabQuanLySach.Controls.Add(this.btnLuu_qls);
            this.tabQuanLySach.Controls.Add(this.btnXoa_qls);
            this.tabQuanLySach.Controls.Add(this.picQuanLySach);
            this.tabQuanLySach.Controls.Add(this.btnSua_qls);
            this.tabQuanLySach.Location = new System.Drawing.Point(4, 33);
            this.tabQuanLySach.Margin = new System.Windows.Forms.Padding(4);
            this.tabQuanLySach.Name = "tabQuanLySach";
            this.tabQuanLySach.Padding = new System.Windows.Forms.Padding(4);
            this.tabQuanLySach.Size = new System.Drawing.Size(1167, 614);
            this.tabQuanLySach.TabIndex = 1;
            this.tabQuanLySach.Text = "Quản lý sách";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(799, 57);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(172, 30);
            this.numericUpDown1.TabIndex = 60;
            // 
            // cboTheLoai_qls
            // 
            this.cboTheLoai_qls.FormattingEnabled = true;
            this.cboTheLoai_qls.Location = new System.Drawing.Point(799, 6);
            this.cboTheLoai_qls.Margin = new System.Windows.Forms.Padding(4);
            this.cboTheLoai_qls.Name = "cboTheLoai_qls";
            this.cboTheLoai_qls.Size = new System.Drawing.Size(169, 32);
            this.cboTheLoai_qls.TabIndex = 59;
            // 
            // lblTheLoai_qls
            // 
            this.lblTheLoai_qls.AutoSize = true;
            this.lblTheLoai_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTheLoai_qls.Location = new System.Drawing.Point(684, 11);
            this.lblTheLoai_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTheLoai_qls.Name = "lblTheLoai_qls";
            this.lblTheLoai_qls.Size = new System.Drawing.Size(87, 22);
            this.lblTheLoai_qls.TabIndex = 58;
            this.lblTheLoai_qls.Text = "Thể loại :";
            // 
            // lblMaSach_qls
            // 
            this.lblMaSach_qls.AutoSize = true;
            this.lblMaSach_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSach_qls.Location = new System.Drawing.Point(15, 11);
            this.lblMaSach_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaSach_qls.Name = "lblMaSach_qls";
            this.lblMaSach_qls.Size = new System.Drawing.Size(90, 22);
            this.lblMaSach_qls.TabIndex = 46;
            this.lblMaSach_qls.Text = "Mã sách :";
            // 
            // lblDonGia_qls
            // 
            this.lblDonGia_qls.AutoSize = true;
            this.lblDonGia_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDonGia_qls.Location = new System.Drawing.Point(15, 107);
            this.lblDonGia_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDonGia_qls.Name = "lblDonGia_qls";
            this.lblDonGia_qls.Size = new System.Drawing.Size(86, 22);
            this.lblDonGia_qls.TabIndex = 56;
            this.lblDonGia_qls.Text = "Đơn giá :";
            // 
            // txtDonGia_qls
            // 
            this.txtDonGia_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGia_qls.Location = new System.Drawing.Point(145, 103);
            this.txtDonGia_qls.Margin = new System.Windows.Forms.Padding(4);
            this.txtDonGia_qls.Name = "txtDonGia_qls";
            this.txtDonGia_qls.Size = new System.Drawing.Size(169, 29);
            this.txtDonGia_qls.TabIndex = 57;
            // 
            // txtMaSach_qls
            // 
            this.txtMaSach_qls.Enabled = false;
            this.txtMaSach_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSach_qls.Location = new System.Drawing.Point(145, 7);
            this.txtMaSach_qls.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaSach_qls.Name = "txtMaSach_qls";
            this.txtMaSach_qls.Size = new System.Drawing.Size(169, 29);
            this.txtMaSach_qls.TabIndex = 47;
            // 
            // lblSoLuong_qls
            // 
            this.lblSoLuong_qls.AutoSize = true;
            this.lblSoLuong_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong_qls.Location = new System.Drawing.Point(687, 64);
            this.lblSoLuong_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSoLuong_qls.Name = "lblSoLuong_qls";
            this.lblSoLuong_qls.Size = new System.Drawing.Size(99, 22);
            this.lblSoLuong_qls.TabIndex = 54;
            this.lblSoLuong_qls.Text = "Số lượng :";
            // 
            // txtTenSach_qls
            // 
            this.txtTenSach_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenSach_qls.Location = new System.Drawing.Point(145, 58);
            this.txtTenSach_qls.Margin = new System.Windows.Forms.Padding(4);
            this.txtTenSach_qls.Name = "txtTenSach_qls";
            this.txtTenSach_qls.Size = new System.Drawing.Size(169, 29);
            this.txtTenSach_qls.TabIndex = 49;
            // 
            // lblTenSach_qls
            // 
            this.lblTenSach_qls.AutoSize = true;
            this.lblTenSach_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSach_qls.Location = new System.Drawing.Point(15, 62);
            this.lblTenSach_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTenSach_qls.Name = "lblTenSach_qls";
            this.lblTenSach_qls.Size = new System.Drawing.Size(98, 22);
            this.lblTenSach_qls.TabIndex = 48;
            this.lblTenSach_qls.Text = "Tên sách :";
            // 
            // lblNamXuatBan_qls
            // 
            this.lblNamXuatBan_qls.AutoSize = true;
            this.lblNamXuatBan_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNamXuatBan_qls.Location = new System.Drawing.Point(340, 62);
            this.lblNamXuatBan_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNamXuatBan_qls.Name = "lblNamXuatBan_qls";
            this.lblNamXuatBan_qls.Size = new System.Drawing.Size(134, 22);
            this.lblNamXuatBan_qls.TabIndex = 52;
            this.lblNamXuatBan_qls.Text = "Năm xuất bản :";
            // 
            // txtTacGia_qls
            // 
            this.txtTacGia_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTacGia_qls.Location = new System.Drawing.Point(489, 7);
            this.txtTacGia_qls.Margin = new System.Windows.Forms.Padding(4);
            this.txtTacGia_qls.Name = "txtTacGia_qls";
            this.txtTacGia_qls.Size = new System.Drawing.Size(169, 29);
            this.txtTacGia_qls.TabIndex = 51;
            // 
            // txtNamXuatBan_qls
            // 
            this.txtNamXuatBan_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamXuatBan_qls.Location = new System.Drawing.Point(489, 58);
            this.txtNamXuatBan_qls.Margin = new System.Windows.Forms.Padding(4);
            this.txtNamXuatBan_qls.Name = "txtNamXuatBan_qls";
            this.txtNamXuatBan_qls.Size = new System.Drawing.Size(169, 29);
            this.txtNamXuatBan_qls.TabIndex = 53;
            // 
            // lblTacGia_qls
            // 
            this.lblTacGia_qls.AutoSize = true;
            this.lblTacGia_qls.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTacGia_qls.Location = new System.Drawing.Point(340, 11);
            this.lblTacGia_qls.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTacGia_qls.Name = "lblTacGia_qls";
            this.lblTacGia_qls.Size = new System.Drawing.Size(82, 22);
            this.lblTacGia_qls.TabIndex = 50;
            this.lblTacGia_qls.Text = "Tác giả :";
            // 
            // dgvQuanLySach
            // 
            this.dgvQuanLySach.BackgroundColor = System.Drawing.Color.Ivory;
            this.dgvQuanLySach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuanLySach.Location = new System.Drawing.Point(4, 242);
            this.dgvQuanLySach.Margin = new System.Windows.Forms.Padding(4);
            this.dgvQuanLySach.Name = "dgvQuanLySach";
            this.dgvQuanLySach.RowHeadersWidth = 51;
            this.dgvQuanLySach.Size = new System.Drawing.Size(1156, 350);
            this.dgvQuanLySach.TabIndex = 39;
            // 
            // btnNhapSach_qls
            // 
            this.btnNhapSach_qls.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnNhapSach_qls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNhapSach_qls.Image = global::Nha_sach_506.Properties.Resources.book_add_icon;
            this.btnNhapSach_qls.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNhapSach_qls.Location = new System.Drawing.Point(979, 15);
            this.btnNhapSach_qls.Margin = new System.Windows.Forms.Padding(4);
            this.btnNhapSach_qls.Name = "btnNhapSach_qls";
            this.btnNhapSach_qls.Size = new System.Drawing.Size(179, 65);
            this.btnNhapSach_qls.TabIndex = 38;
            this.btnNhapSach_qls.Text = "Nhập sách";
            this.btnNhapSach_qls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNhapSach_qls.UseVisualStyleBackColor = false;
            // 
            // btnHuyBo_qls
            // 
            this.btnHuyBo_qls.BackColor = System.Drawing.Color.Tomato;
            this.btnHuyBo_qls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHuyBo_qls.Image = global::Nha_sach_506.Properties.Resources.cancel_dialogue_icone_4250_48;
            this.btnHuyBo_qls.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHuyBo_qls.Location = new System.Drawing.Point(551, 148);
            this.btnHuyBo_qls.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyBo_qls.Name = "btnHuyBo_qls";
            this.btnHuyBo_qls.Size = new System.Drawing.Size(159, 65);
            this.btnHuyBo_qls.TabIndex = 37;
            this.btnHuyBo_qls.Text = "Hủy bỏ";
            this.btnHuyBo_qls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHuyBo_qls.UseVisualStyleBackColor = false;
            // 
            // btnLuu_qls
            // 
            this.btnLuu_qls.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnLuu_qls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLuu_qls.Image = global::Nha_sach_506.Properties.Resources.save_48x48;
            this.btnLuu_qls.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLuu_qls.Location = new System.Drawing.Point(353, 148);
            this.btnLuu_qls.Margin = new System.Windows.Forms.Padding(4);
            this.btnLuu_qls.Name = "btnLuu_qls";
            this.btnLuu_qls.Size = new System.Drawing.Size(136, 65);
            this.btnLuu_qls.TabIndex = 36;
            this.btnLuu_qls.Text = "Lưu";
            this.btnLuu_qls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLuu_qls.UseVisualStyleBackColor = false;
            // 
            // btnXoa_qls
            // 
            this.btnXoa_qls.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnXoa_qls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnXoa_qls.Image = global::Nha_sach_506.Properties.Resources.cancel_icon;
            this.btnXoa_qls.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnXoa_qls.Location = new System.Drawing.Point(185, 148);
            this.btnXoa_qls.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa_qls.Name = "btnXoa_qls";
            this.btnXoa_qls.Size = new System.Drawing.Size(119, 65);
            this.btnXoa_qls.TabIndex = 35;
            this.btnXoa_qls.Text = "Xóa";
            this.btnXoa_qls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnXoa_qls.UseVisualStyleBackColor = false;
            // 
            // picQuanLySach
            // 
            this.picQuanLySach.Image = global::Nha_sach_506.Properties.Resources.Books_on_handtruck_banner;
            this.picQuanLySach.Location = new System.Drawing.Point(741, 107);
            this.picQuanLySach.Margin = new System.Windows.Forms.Padding(4);
            this.picQuanLySach.Name = "picQuanLySach";
            this.picQuanLySach.Size = new System.Drawing.Size(415, 106);
            this.picQuanLySach.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQuanLySach.TabIndex = 34;
            this.picQuanLySach.TabStop = false;
            // 
            // btnSua_qls
            // 
            this.btnSua_qls.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSua_qls.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSua_qls.Image = global::Nha_sach_506.Properties.Resources.pencil_icon;
            this.btnSua_qls.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSua_qls.Location = new System.Drawing.Point(21, 148);
            this.btnSua_qls.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua_qls.Name = "btnSua_qls";
            this.btnSua_qls.Size = new System.Drawing.Size(119, 65);
            this.btnSua_qls.TabIndex = 32;
            this.btnSua_qls.Text = "Sửa";
            this.btnSua_qls.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSua_qls.UseVisualStyleBackColor = false;
            // 
            // tabTraCuu
            // 
            this.tabTraCuu.Controls.Add(this.grpKhachHang_tc);
            this.tabTraCuu.Controls.Add(this.grpHoaDon_tc);
            this.tabTraCuu.Controls.Add(this.grpSach_tc);
            this.tabTraCuu.Controls.Add(this.dgvTraCuu);
            this.tabTraCuu.Controls.Add(this.txtTuKhoa);
            this.tabTraCuu.Controls.Add(this.lblTuKhoa);
            this.tabTraCuu.Controls.Add(this.picTraCuu);
            this.tabTraCuu.Controls.Add(this.PictureBox4);
            this.tabTraCuu.Location = new System.Drawing.Point(4, 33);
            this.tabTraCuu.Margin = new System.Windows.Forms.Padding(4);
            this.tabTraCuu.Name = "tabTraCuu";
            this.tabTraCuu.Padding = new System.Windows.Forms.Padding(4);
            this.tabTraCuu.Size = new System.Drawing.Size(1167, 614);
            this.tabTraCuu.TabIndex = 2;
            this.tabTraCuu.Text = "Tra cứu";
            this.tabTraCuu.UseVisualStyleBackColor = true;
            // 
            // grpKhachHang_tc
            // 
            this.grpKhachHang_tc.Controls.Add(this.rbtKH_tc);
            this.grpKhachHang_tc.Controls.Add(this.chkKhachNo);
            this.grpKhachHang_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpKhachHang_tc.Location = new System.Drawing.Point(548, 91);
            this.grpKhachHang_tc.Margin = new System.Windows.Forms.Padding(4);
            this.grpKhachHang_tc.Name = "grpKhachHang_tc";
            this.grpKhachHang_tc.Padding = new System.Windows.Forms.Padding(4);
            this.grpKhachHang_tc.Size = new System.Drawing.Size(211, 98);
            this.grpKhachHang_tc.TabIndex = 46;
            this.grpKhachHang_tc.TabStop = false;
            this.grpKhachHang_tc.Text = "Tra cứu Khách hàng";
            // 
            // rbtKH_tc
            // 
            this.rbtKH_tc.AutoSize = true;
            this.rbtKH_tc.Location = new System.Drawing.Point(20, 26);
            this.rbtKH_tc.Margin = new System.Windows.Forms.Padding(4);
            this.rbtKH_tc.Name = "rbtKH_tc";
            this.rbtKH_tc.Size = new System.Drawing.Size(124, 23);
            this.rbtKH_tc.TabIndex = 44;
            this.rbtKH_tc.TabStop = true;
            this.rbtKH_tc.Text = "Khách hàng";
            this.rbtKH_tc.UseVisualStyleBackColor = true;
            // 
            // chkKhachNo
            // 
            this.chkKhachNo.AutoSize = true;
            this.chkKhachNo.Location = new System.Drawing.Point(20, 58);
            this.chkKhachNo.Margin = new System.Windows.Forms.Padding(4);
            this.chkKhachNo.Name = "chkKhachNo";
            this.chkKhachNo.Size = new System.Drawing.Size(108, 23);
            this.chkKhachNo.TabIndex = 12;
            this.chkKhachNo.Text = "Khách nợ";
            this.chkKhachNo.UseVisualStyleBackColor = true;
            // 
            // grpHoaDon_tc
            // 
            this.grpHoaDon_tc.Controls.Add(this.rbtHD_tc);
            this.grpHoaDon_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpHoaDon_tc.Location = new System.Drawing.Point(284, 91);
            this.grpHoaDon_tc.Margin = new System.Windows.Forms.Padding(4);
            this.grpHoaDon_tc.Name = "grpHoaDon_tc";
            this.grpHoaDon_tc.Padding = new System.Windows.Forms.Padding(4);
            this.grpHoaDon_tc.Size = new System.Drawing.Size(211, 98);
            this.grpHoaDon_tc.TabIndex = 45;
            this.grpHoaDon_tc.TabStop = false;
            this.grpHoaDon_tc.Text = "Tra cứu Hóa đơn";
            // 
            // rbtHD_tc
            // 
            this.rbtHD_tc.AutoSize = true;
            this.rbtHD_tc.Location = new System.Drawing.Point(31, 26);
            this.rbtHD_tc.Margin = new System.Windows.Forms.Padding(4);
            this.rbtHD_tc.Name = "rbtHD_tc";
            this.rbtHD_tc.Size = new System.Drawing.Size(98, 23);
            this.rbtHD_tc.TabIndex = 44;
            this.rbtHD_tc.TabStop = true;
            this.rbtHD_tc.Text = "Hoá đơn";
            this.rbtHD_tc.UseVisualStyleBackColor = true;
            // 
            // grpSach_tc
            // 
            this.grpSach_tc.Controls.Add(this.rbtS_tc);
            this.grpSach_tc.Controls.Add(this.chkConHang);
            this.grpSach_tc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSach_tc.Location = new System.Drawing.Point(27, 91);
            this.grpSach_tc.Margin = new System.Windows.Forms.Padding(4);
            this.grpSach_tc.Name = "grpSach_tc";
            this.grpSach_tc.Padding = new System.Windows.Forms.Padding(4);
            this.grpSach_tc.Size = new System.Drawing.Size(211, 98);
            this.grpSach_tc.TabIndex = 44;
            this.grpSach_tc.TabStop = false;
            this.grpSach_tc.Text = "Tra cứu Sách";
            // 
            // rbtS_tc
            // 
            this.rbtS_tc.AutoSize = true;
            this.rbtS_tc.Location = new System.Drawing.Point(25, 26);
            this.rbtS_tc.Margin = new System.Windows.Forms.Padding(4);
            this.rbtS_tc.Name = "rbtS_tc";
            this.rbtS_tc.Size = new System.Drawing.Size(69, 23);
            this.rbtS_tc.TabIndex = 44;
            this.rbtS_tc.TabStop = true;
            this.rbtS_tc.Text = "Sách";
            this.rbtS_tc.UseVisualStyleBackColor = true;
            // 
            // chkConHang
            // 
            this.chkConHang.AutoSize = true;
            this.chkConHang.Location = new System.Drawing.Point(25, 58);
            this.chkConHang.Margin = new System.Windows.Forms.Padding(4);
            this.chkConHang.Name = "chkConHang";
            this.chkConHang.Size = new System.Drawing.Size(107, 23);
            this.chkConHang.TabIndex = 10;
            this.chkConHang.Text = "Còn hàng";
            this.chkConHang.UseVisualStyleBackColor = true;
            // 
            // dgvTraCuu
            // 
            this.dgvTraCuu.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvTraCuu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTraCuu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvTraCuu.Location = new System.Drawing.Point(4, 224);
            this.dgvTraCuu.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTraCuu.Name = "dgvTraCuu";
            this.dgvTraCuu.RowHeadersWidth = 51;
            this.dgvTraCuu.Size = new System.Drawing.Size(1159, 386);
            this.dgvTraCuu.TabIndex = 11;
            // 
            // txtTuKhoa
            // 
            this.txtTuKhoa.Location = new System.Drawing.Point(28, 52);
            this.txtTuKhoa.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuKhoa.Name = "txtTuKhoa";
            this.txtTuKhoa.Size = new System.Drawing.Size(617, 30);
            this.txtTuKhoa.TabIndex = 8;
            // 
            // lblTuKhoa
            // 
            this.lblTuKhoa.AutoSize = true;
            this.lblTuKhoa.Location = new System.Drawing.Point(23, 25);
            this.lblTuKhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuKhoa.Name = "lblTuKhoa";
            this.lblTuKhoa.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblTuKhoa.Size = new System.Drawing.Size(102, 24);
            this.lblTuKhoa.TabIndex = 7;
            this.lblTuKhoa.Text = "Từ khóa :";
            // 
            // picTraCuu
            // 
            this.picTraCuu.Image = global::Nha_sach_506.Properties.Resources.stock_control;
            this.picTraCuu.Location = new System.Drawing.Point(951, 25);
            this.picTraCuu.Margin = new System.Windows.Forms.Padding(4);
            this.picTraCuu.Name = "picTraCuu";
            this.picTraCuu.Size = new System.Drawing.Size(151, 119);
            this.picTraCuu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTraCuu.TabIndex = 9;
            this.picTraCuu.TabStop = false;
            // 
            // PictureBox4
            // 
            this.PictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.PictureBox4.Location = new System.Drawing.Point(4, 4);
            this.PictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox4.Name = "PictureBox4";
            this.PictureBox4.Size = new System.Drawing.Size(1159, 203);
            this.PictureBox4.TabIndex = 6;
            this.PictureBox4.TabStop = false;
            // 
            // tabTruyVanDuLieu
            // 
            this.tabTruyVanDuLieu.Controls.Add(this.txtNam_tvdl);
            this.tabTruyVanDuLieu.Controls.Add(this.lblNam);
            this.tabTruyVanDuLieu.Controls.Add(this.txtThang_tvdl);
            this.tabTruyVanDuLieu.Controls.Add(this.lblThang);
            this.tabTruyVanDuLieu.Controls.Add(this.btnBaoCaoNo);
            this.tabTruyVanDuLieu.Controls.Add(this.btnBaoCaoTon);
            this.tabTruyVanDuLieu.Controls.Add(this.picTruyVanDuLieu);
            this.tabTruyVanDuLieu.Location = new System.Drawing.Point(4, 33);
            this.tabTruyVanDuLieu.Margin = new System.Windows.Forms.Padding(4);
            this.tabTruyVanDuLieu.Name = "tabTruyVanDuLieu";
            this.tabTruyVanDuLieu.Padding = new System.Windows.Forms.Padding(4);
            this.tabTruyVanDuLieu.Size = new System.Drawing.Size(1167, 614);
            this.tabTruyVanDuLieu.TabIndex = 4;
            this.tabTruyVanDuLieu.Text = "Truy vấn dữ liệu";
            this.tabTruyVanDuLieu.UseVisualStyleBackColor = true;
            // 
            // txtNam_tvdl
            // 
            this.txtNam_tvdl.Location = new System.Drawing.Point(348, 26);
            this.txtNam_tvdl.Margin = new System.Windows.Forms.Padding(4);
            this.txtNam_tvdl.Name = "txtNam_tvdl";
            this.txtNam_tvdl.Size = new System.Drawing.Size(132, 30);
            this.txtNam_tvdl.TabIndex = 12;
            // 
            // lblNam
            // 
            this.lblNam.AutoSize = true;
            this.lblNam.Location = new System.Drawing.Point(288, 30);
            this.lblNam.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNam.Name = "lblNam";
            this.lblNam.Size = new System.Drawing.Size(52, 24);
            this.lblNam.TabIndex = 11;
            this.lblNam.Text = "Năm";
            // 
            // txtThang_tvdl
            // 
            this.txtThang_tvdl.Location = new System.Drawing.Point(119, 26);
            this.txtThang_tvdl.Margin = new System.Windows.Forms.Padding(4);
            this.txtThang_tvdl.Name = "txtThang_tvdl";
            this.txtThang_tvdl.Size = new System.Drawing.Size(132, 30);
            this.txtThang_tvdl.TabIndex = 10;
            // 
            // lblThang
            // 
            this.lblThang.AutoSize = true;
            this.lblThang.Location = new System.Drawing.Point(40, 30);
            this.lblThang.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThang.Name = "lblThang";
            this.lblThang.Size = new System.Drawing.Size(70, 24);
            this.lblThang.TabIndex = 9;
            this.lblThang.Text = "Tháng";
            // 
            // btnBaoCaoNo
            // 
            this.btnBaoCaoNo.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnBaoCaoNo.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaoCaoNo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBaoCaoNo.Image = global::Nha_sach_506.Properties.Resources.payment_icon;
            this.btnBaoCaoNo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBaoCaoNo.Location = new System.Drawing.Point(348, 78);
            this.btnBaoCaoNo.Margin = new System.Windows.Forms.Padding(4);
            this.btnBaoCaoNo.Name = "btnBaoCaoNo";
            this.btnBaoCaoNo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBaoCaoNo.Size = new System.Drawing.Size(241, 101);
            this.btnBaoCaoNo.TabIndex = 8;
            this.btnBaoCaoNo.Text = "Báo cáo nợ";
            this.btnBaoCaoNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBaoCaoNo.UseVisualStyleBackColor = false;
            // 
            // btnBaoCaoTon
            // 
            this.btnBaoCaoTon.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnBaoCaoTon.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaoCaoTon.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBaoCaoTon.Image = global::Nha_sach_506.Properties.Resources.chart_icon;
            this.btnBaoCaoTon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBaoCaoTon.Location = new System.Drawing.Point(35, 78);
            this.btnBaoCaoTon.Margin = new System.Windows.Forms.Padding(4);
            this.btnBaoCaoTon.Name = "btnBaoCaoTon";
            this.btnBaoCaoTon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBaoCaoTon.Size = new System.Drawing.Size(247, 101);
            this.btnBaoCaoTon.TabIndex = 7;
            this.btnBaoCaoTon.Text = "Báo cáo tồn";
            this.btnBaoCaoTon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBaoCaoTon.UseVisualStyleBackColor = false;
            // 
            // picTruyVanDuLieu
            // 
            this.picTruyVanDuLieu.Image = global::Nha_sach_506.Properties.Resources.j0440966;
            this.picTruyVanDuLieu.Location = new System.Drawing.Point(8, 7);
            this.picTruyVanDuLieu.Margin = new System.Windows.Forms.Padding(4);
            this.picTruyVanDuLieu.Name = "picTruyVanDuLieu";
            this.picTruyVanDuLieu.Size = new System.Drawing.Size(1152, 597);
            this.picTruyVanDuLieu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picTruyVanDuLieu.TabIndex = 1;
            this.picTruyVanDuLieu.TabStop = false;
            // 
            // tabQuyDinh
            // 
            this.tabQuyDinh.Controls.Add(this.rbtNo_qd);
            this.tabQuyDinh.Controls.Add(this.rbtYes_qd);
            this.tabQuyDinh.Controls.Add(this.txtTonMinSauBan_qd);
            this.tabQuyDinh.Controls.Add(this.txtNoMax_qd);
            this.tabQuyDinh.Controls.Add(this.txtTonMin_qd);
            this.tabQuyDinh.Controls.Add(this.txtNhapMin_qd);
            this.tabQuyDinh.Controls.Add(this.label8);
            this.tabQuyDinh.Controls.Add(this.label7);
            this.tabQuyDinh.Controls.Add(this.label6);
            this.tabQuyDinh.Controls.Add(this.label5);
            this.tabQuyDinh.Controls.Add(this.label3);
            this.tabQuyDinh.Controls.Add(this.lblQuydinh);
            this.tabQuyDinh.Controls.Add(this.btnApDung_qd);
            this.tabQuyDinh.Controls.Add(this.btnHuyBo_qd);
            this.tabQuyDinh.Controls.Add(this.btnSua_qd);
            this.tabQuyDinh.Location = new System.Drawing.Point(4, 33);
            this.tabQuyDinh.Margin = new System.Windows.Forms.Padding(4);
            this.tabQuyDinh.Name = "tabQuyDinh";
            this.tabQuyDinh.Padding = new System.Windows.Forms.Padding(4);
            this.tabQuyDinh.Size = new System.Drawing.Size(1167, 614);
            this.tabQuyDinh.TabIndex = 5;
            this.tabQuyDinh.Text = "Quy định";
            this.tabQuyDinh.UseVisualStyleBackColor = true;
            // 
            // rbtNo_qd
            // 
            this.rbtNo_qd.AutoSize = true;
            this.rbtNo_qd.Location = new System.Drawing.Point(729, 240);
            this.rbtNo_qd.Margin = new System.Windows.Forms.Padding(4);
            this.rbtNo_qd.Name = "rbtNo_qd";
            this.rbtNo_qd.Size = new System.Drawing.Size(90, 28);
            this.rbtNo_qd.TabIndex = 44;
            this.rbtNo_qd.TabStop = true;
            this.rbtNo_qd.Text = "không";
            this.rbtNo_qd.UseVisualStyleBackColor = true;
            // 
            // rbtYes_qd
            // 
            this.rbtYes_qd.AutoSize = true;
            this.rbtYes_qd.Location = new System.Drawing.Point(645, 240);
            this.rbtYes_qd.Margin = new System.Windows.Forms.Padding(4);
            this.rbtYes_qd.Name = "rbtYes_qd";
            this.rbtYes_qd.Size = new System.Drawing.Size(54, 28);
            this.rbtYes_qd.TabIndex = 44;
            this.rbtYes_qd.TabStop = true;
            this.rbtYes_qd.Text = "có";
            this.rbtYes_qd.UseVisualStyleBackColor = true;
            // 
            // txtTonMinSauBan_qd
            // 
            this.txtTonMinSauBan_qd.Location = new System.Drawing.Point(644, 197);
            this.txtTonMinSauBan_qd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTonMinSauBan_qd.Name = "txtTonMinSauBan_qd";
            this.txtTonMinSauBan_qd.Size = new System.Drawing.Size(267, 30);
            this.txtTonMinSauBan_qd.TabIndex = 43;
            // 
            // txtNoMax_qd
            // 
            this.txtNoMax_qd.Location = new System.Drawing.Point(644, 158);
            this.txtNoMax_qd.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoMax_qd.Name = "txtNoMax_qd";
            this.txtNoMax_qd.Size = new System.Drawing.Size(267, 30);
            this.txtNoMax_qd.TabIndex = 43;
            // 
            // txtTonMin_qd
            // 
            this.txtTonMin_qd.Location = new System.Drawing.Point(644, 118);
            this.txtTonMin_qd.Margin = new System.Windows.Forms.Padding(4);
            this.txtTonMin_qd.Name = "txtTonMin_qd";
            this.txtTonMin_qd.Size = new System.Drawing.Size(267, 30);
            this.txtTonMin_qd.TabIndex = 43;
            // 
            // txtNhapMin_qd
            // 
            this.txtNhapMin_qd.Location = new System.Drawing.Point(644, 75);
            this.txtNhapMin_qd.Margin = new System.Windows.Forms.Padding(4);
            this.txtNhapMin_qd.Name = "txtNhapMin_qd";
            this.txtNhapMin_qd.Size = new System.Drawing.Size(267, 30);
            this.txtNhapMin_qd.TabIndex = 43;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(83, 242);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(518, 24);
            this.label8.TabIndex = 42;
            this.label8.Text = "Sử dụng số tiền thu không vượt quá số tiền đang nợ:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(83, 207);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(252, 24);
            this.label7.TabIndex = 41;
            this.label7.Text = "Tồn tối thiểu sau khi bán:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(83, 166);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 24);
            this.label6.TabIndex = 41;
            this.label6.Text = "Tiền nợ tối đa:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 127);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 24);
            this.label5.TabIndex = 41;
            this.label5.Text = "Tồn tối thiểu:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(83, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 24);
            this.label3.TabIndex = 41;
            this.label3.Text = "Nhập tối thiểu";
            // 
            // lblQuydinh
            // 
            this.lblQuydinh.AutoSize = true;
            this.lblQuydinh.Location = new System.Drawing.Point(37, 39);
            this.lblQuydinh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuydinh.Name = "lblQuydinh";
            this.lblQuydinh.Size = new System.Drawing.Size(109, 24);
            this.lblQuydinh.TabIndex = 10;
            this.lblQuydinh.Text = "Quy định :";
            // 
            // btnApDung_qd
            // 
            this.btnApDung_qd.BackColor = System.Drawing.Color.LimeGreen;
            this.btnApDung_qd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnApDung_qd.Image = global::Nha_sach_506.Properties.Resources.check_yes_ok_icone_7166_48;
            this.btnApDung_qd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApDung_qd.Location = new System.Drawing.Point(644, 298);
            this.btnApDung_qd.Margin = new System.Windows.Forms.Padding(4);
            this.btnApDung_qd.Name = "btnApDung_qd";
            this.btnApDung_qd.Size = new System.Drawing.Size(168, 65);
            this.btnApDung_qd.TabIndex = 40;
            this.btnApDung_qd.Text = "Áp dụng";
            this.btnApDung_qd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnApDung_qd.UseVisualStyleBackColor = false;
            // 
            // btnHuyBo_qd
            // 
            this.btnHuyBo_qd.BackColor = System.Drawing.Color.Tomato;
            this.btnHuyBo_qd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHuyBo_qd.Image = global::Nha_sach_506.Properties.Resources.cancel_dialogue_icone_4250_48;
            this.btnHuyBo_qd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHuyBo_qd.Location = new System.Drawing.Point(981, 298);
            this.btnHuyBo_qd.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyBo_qd.Name = "btnHuyBo_qd";
            this.btnHuyBo_qd.Size = new System.Drawing.Size(159, 65);
            this.btnHuyBo_qd.TabIndex = 39;
            this.btnHuyBo_qd.Text = "Hủy bỏ";
            this.btnHuyBo_qd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHuyBo_qd.UseVisualStyleBackColor = false;
            // 
            // btnSua_qd
            // 
            this.btnSua_qd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnSua_qd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSua_qd.Image = global::Nha_sach_506.Properties.Resources.pencil_icon;
            this.btnSua_qd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSua_qd.Location = new System.Drawing.Point(841, 298);
            this.btnSua_qd.Margin = new System.Windows.Forms.Padding(4);
            this.btnSua_qd.Name = "btnSua_qd";
            this.btnSua_qd.Size = new System.Drawing.Size(119, 65);
            this.btnSua_qd.TabIndex = 38;
            this.btnSua_qd.Text = "Sửa";
            this.btnSua_qd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSua_qd.UseVisualStyleBackColor = false;
            // 
            // lblChucVu
            // 
            this.lblChucVu.AutoSize = true;
            this.lblChucVu.BackColor = System.Drawing.Color.Transparent;
            this.lblChucVu.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChucVu.Location = new System.Drawing.Point(964, 144);
            this.lblChucVu.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChucVu.Name = "lblChucVu";
            this.lblChucVu.Size = new System.Drawing.Size(81, 22);
            this.lblChucVu.TabIndex = 23;
            this.lblChucVu.Text = "Chức vụ";
            // 
            // lblTenNguoiDung
            // 
            this.lblTenNguoiDung.AutoSize = true;
            this.lblTenNguoiDung.BackColor = System.Drawing.Color.Transparent;
            this.lblTenNguoiDung.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiDung.Location = new System.Drawing.Point(787, 144);
            this.lblTenNguoiDung.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTenNguoiDung.Name = "lblTenNguoiDung";
            this.lblTenNguoiDung.Size = new System.Drawing.Size(145, 22);
            this.lblTenNguoiDung.TabIndex = 22;
            this.lblTenNguoiDung.Text = "Tên người dùng";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(883, 838);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(266, 17);
            this.Label4.TabIndex = 42;
            this.Label4.Text = "© Copyright 2016. 506 HUTECH Group";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(1071, 142);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(74, 24);
            this.lblDate.TabIndex = 44;
            this.lblDate.Text = "01/01/16";
            this.lblDate.Click += new System.EventHandler(this.lblDate_Click);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.BackColor = System.Drawing.Color.Tomato;
            this.btnDangXuat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDangXuat.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangXuat.Image = global::Nha_sach_506.Properties.Resources.exit;
            this.btnDangXuat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDangXuat.Location = new System.Drawing.Point(972, 60);
            this.btnDangXuat.Margin = new System.Windows.Forms.Padding(4);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(177, 64);
            this.btnDangXuat.TabIndex = 43;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDangXuat.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Nha_sach_506.Properties.Resources.banner_bookshelf;
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1175, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // frmNhaSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1180, 858);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.btnDangXuat);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.lblChucVu);
            this.Controls.Add(this.lblTenNguoiDung);
            this.Controls.Add(this.tabNhaSach);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmNhaSach";
            this.Text = "Phần mềm quản lý Nhà sách 506 - Phiên bản 2";
            this.Load += new System.EventHandler(this.frmNhaSach_Load);
            this.tabNhaSach.ResumeLayout(false);
            this.tabTrangChu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picTrangChu)).EndInit();
            this.tabQuanLySach.ResumeLayout(false);
            this.tabQuanLySach.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuanLySach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQuanLySach)).EndInit();
            this.tabTraCuu.ResumeLayout(false);
            this.tabTraCuu.PerformLayout();
            this.grpKhachHang_tc.ResumeLayout(false);
            this.grpKhachHang_tc.PerformLayout();
            this.grpHoaDon_tc.ResumeLayout(false);
            this.grpHoaDon_tc.PerformLayout();
            this.grpSach_tc.ResumeLayout(false);
            this.grpSach_tc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTraCuu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTraCuu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox4)).EndInit();
            this.tabTruyVanDuLieu.ResumeLayout(false);
            this.tabTruyVanDuLieu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTruyVanDuLieu)).EndInit();
            this.tabQuyDinh.ResumeLayout(false);
            this.tabQuyDinh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabNhaSach;
        private System.Windows.Forms.TabPage tabTrangChu;
        private System.Windows.Forms.PictureBox picTrangChu;
        private System.Windows.Forms.TabPage tabQuanLySach;
        internal System.Windows.Forms.PictureBox picQuanLySach;
        internal System.Windows.Forms.Button btnSua_qls;
        private System.Windows.Forms.DataGridView dgvQuanLySach;
        internal System.Windows.Forms.Button btnNhapSach_qls;
        internal System.Windows.Forms.Button btnHuyBo_qls;
        internal System.Windows.Forms.Button btnLuu_qls;
        internal System.Windows.Forms.Button btnXoa_qls;
        private System.Windows.Forms.TabPage tabTraCuu;
        private System.Windows.Forms.TabPage tabTruyVanDuLieu;
        internal System.Windows.Forms.DataGridView dgvTraCuu;
        internal System.Windows.Forms.TextBox txtTuKhoa;
        internal System.Windows.Forms.Label lblTuKhoa;
        internal System.Windows.Forms.PictureBox picTraCuu;
        internal System.Windows.Forms.PictureBox PictureBox4;
        internal System.Windows.Forms.TextBox txtNam_tvdl;
        internal System.Windows.Forms.Label lblNam;
        internal System.Windows.Forms.TextBox txtThang_tvdl;
        internal System.Windows.Forms.Label lblThang;
        internal System.Windows.Forms.Button btnBaoCaoNo;
        internal System.Windows.Forms.Button btnBaoCaoTon;
        internal System.Windows.Forms.PictureBox picTruyVanDuLieu;
        internal System.Windows.Forms.Label lblChucVu;
        internal System.Windows.Forms.Label lblTenNguoiDung;
        internal System.Windows.Forms.Label Label4;
        private System.Windows.Forms.TabPage tabQuyDinh;
        internal System.Windows.Forms.Button btnApDung_qd;
        internal System.Windows.Forms.Button btnHuyBo_qd;
        internal System.Windows.Forms.Button btnSua_qd;
        internal System.Windows.Forms.Label lblQuydinh;
        internal System.Windows.Forms.Button btnDangXuat;
        internal System.Windows.Forms.ComboBox cboTheLoai_qls;
        internal System.Windows.Forms.Label lblTheLoai_qls;
        internal System.Windows.Forms.Label lblMaSach_qls;
        internal System.Windows.Forms.Label lblDonGia_qls;
        internal System.Windows.Forms.TextBox txtDonGia_qls;
        internal System.Windows.Forms.TextBox txtMaSach_qls;
        internal System.Windows.Forms.Label lblSoLuong_qls;
        internal System.Windows.Forms.TextBox txtTenSach_qls;
        internal System.Windows.Forms.Label lblTenSach_qls;
        internal System.Windows.Forms.Label lblNamXuatBan_qls;
        internal System.Windows.Forms.TextBox txtTacGia_qls;
        internal System.Windows.Forms.TextBox txtNamXuatBan_qls;
        internal System.Windows.Forms.Label lblTacGia_qls;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.RadioButton rbtNo_qd;
        private System.Windows.Forms.RadioButton rbtYes_qd;
        private System.Windows.Forms.TextBox txtTonMinSauBan_qd;
        private System.Windows.Forms.TextBox txtNoMax_qd;
        private System.Windows.Forms.TextBox txtTonMin_qd;
        private System.Windows.Forms.TextBox txtNhapMin_qd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox grpKhachHang_tc;
        private System.Windows.Forms.RadioButton rbtKH_tc;
        internal System.Windows.Forms.CheckBox chkKhachNo;
        private System.Windows.Forms.GroupBox grpHoaDon_tc;
        private System.Windows.Forms.RadioButton rbtHD_tc;
        private System.Windows.Forms.GroupBox grpSach_tc;
        private System.Windows.Forms.RadioButton rbtS_tc;
        internal System.Windows.Forms.CheckBox chkConHang;

    }
}